package com.jcraft.jzlib;

import java.io.*;

public class TestInflate
{
	public static void main(String[] args)
	{
		try
		{
			String hello = "Hello World!";
			
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			DeflaterOutputStream zOut = new DeflaterOutputStream(out);
			ObjectOutputStream objOut = new ObjectOutputStream(zOut);
			objOut.writeObject(hello);
			zOut.close();
			
			ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
			InflaterInputStream zIn = new InflaterInputStream(in);
			ObjectInputStream objIn = new ObjectInputStream(zIn);
			System.out.println(objIn.readObject());
			objIn.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}