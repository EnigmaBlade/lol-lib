package net.enigmablade.lol.lollib.io.raf;

import java.io.*;
import java.nio.*;
import java.util.*;
import java.util.regex.*;
import com.jcraft.jzlib.*;

import static net.enigmablade.paradoxion.util.Logger.*;

public class RafUtil
{
	private static final String PATH_ADD = "/RADS/projects/lol_game_client/filearchives";
	
	//Regex Extracting
	
	public static File extractPathsMatching(File gamePath, String tempFolderKey, String pathRegex)
	{
		writeToLog("RafUtil # Extracting RAF paths \""+pathRegex+"\"");
		
		//Create output folder
		String outputDir = System.getProperty("java.io.tmpdir")+tempFolderKey;
		File outputDirFile = new File(outputDir);
		outputDirFile.deleteOnExit();
		 
		return extractPathsMatching(gamePath, outputDirFile, pathRegex);
	}
	
	public static File extractPathsMatching(File gamePath, File outputDir, String pathRegex)
	{
		writeToLog("RafUtil # To directory: "+outputDir, 1);
		try
		{
			if(!outputDir.exists())
			{
				outputDir.mkdirs();
				writeToLog("RafUtil # Directory created", 2);
			}
		}
		catch(Exception e)
		{
			writeToLog("RafUtil # Failed to create cache directory", 2, LoggingType.ERROR);
			e.printStackTrace();
			return null;
		}
		
		//Get RAF files
		List<ArchivePair> archives = getArchives(gamePath);
		if(archives == null)
		{
			writeToLog("RafUtil # Failed to get archive directories", LoggingType.ERROR);
			return null;
		}
		
		//Create regex pattern
		final Pattern pathPattern = Pattern.compile(pathRegex);
		
		long allStartTime = System.nanoTime();
		long avgTime = 0;
		int avgCount = 0;
		
		//Extract reach RAF
		for(ArchivePair archive : archives)
		{
			long startTime = System.nanoTime();
			try
			{
				//writeToLog("RafUtil # Reading archive data from RAF: /"+(archive.raf.getParentFile() != null ? archive.raf.getParentFile().getName()+"/" : "")+archive.raf.getName(), 1);
				final File out = outputDir;
				final RafFile raf = new RafFile(archive.raf, archive.dir);
				final File dat = archive.dat;
				
				boolean success = raf.loadPaths();
				if(success)
				{
					new Thread(){
						@Override
						public void run()
						{
							try
							{
								extractFilesMatching(raf, dat, out, pathPattern);
							}
							catch(IOException e)
							{
								writeToLog("Failed to extract files", 2, LoggingType.ERROR);
								writeStackTrace(e);
							}
						}
					}.start();
				}
			}
			catch(IOException e)
			{
				writeToLog("Failed to read file data", 2, LoggingType.ERROR);
				writeStackTrace(e);
			}
			
			long endTime = System.nanoTime();
			avgTime += endTime-startTime;
			avgCount++;
		}
		
		long allEndTime = System.nanoTime();
		long allTime = allEndTime-allStartTime;
		allTime /= 1000000;
		writeToLog("RafUtil # Total time: "+allTime+" ms ("+(allTime/1000.0)+" s)");
		
		avgTime /= avgCount;
		avgTime /= 1000000;
		writeToLog("RafUtil # Avg time: "+avgTime+" ms ("+(avgTime/1000.0)+" s)");
		
		//Don't want any of those icky threads hanging around
		System.gc();
		
		return outputDir;
	}
	
	private static void extractFilesMatching(RafFile raf, File datFile, File outputDir, Pattern pathPattern) throws IOException
	{
		writeToLog("RafUtil # Extracting the files from DAT: /"+datFile.getParentFile().getName()+"/"+datFile.getName(), 1);
		RandomAccessFile dat = null;
		try
		{
			dat = new RandomAccessFile(datFile, "r");
			
			for(FileEntry entry : raf.getFileList())
			{
				//Entry path
				PathEntry entryPath = raf.getPathList().get(entry.pathIndex);
				
				if(pathPattern.matcher(entryPath.path).matches())
				{
					File entryFile = new File(outputDir.getAbsolutePath()+File.separator+entryPath.path);
					//System.out.println("Extracting to: "+entryFile.getAbsolutePath());
					entryFile.getParentFile().mkdirs();
					
					//Entry bytes
					byte[] entryBytes = new byte[entry.size];
					int read = read(dat, entry.offset, entryBytes, false);
					
					if(read > 0)
					{
						//Decompress
						ByteArrayInputStream in = null;
						InflaterInputStream zIn = null;
						FileOutputStream out = null;
						try
						{
							in = new ByteArrayInputStream(entryBytes);
							zIn = new InflaterInputStream(in);
							
							out = new FileOutputStream(entryFile);
							copyStream(zIn, out);
						}
						finally
						{
							if(zIn != null)
								zIn.close();
							if(in != null)
								in.close();
							if(out != null)
								out.close();
						}
					}
				}
			}
		}
		finally
		{
			if(dat != null)
				dat.close();
		}
	}
	
	//Reading
	
	protected static int read(RandomAccessFile file, int offset, byte[] to, boolean revEnd) throws IOException
	{
		file.seek(offset);
		int read = file.read(to);
		if(revEnd)
			reverseEndian(to);
		return read;
	}
	
	protected static byte[] reverseEndian(byte[] bytes)
	{
		for(int i = 0, j = bytes.length-1; i < j; i++, j--)
		{
			byte b = bytes[i];
			bytes[i] = bytes[j];
			bytes[j] = b;
		}
		return bytes;
	}
	
	protected static int toInt(byte[] bytes)
	{
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		IntBuffer intBuffer = buffer.asIntBuffer();
		return intBuffer.get();
	}
	
	protected static void copyStream(InputStream input, OutputStream output) throws IOException
	{
		byte[] buffer = new byte[1024];
		int bytesRead;
		while((bytesRead = input.read(buffer)) != -1)
			output.write(buffer, 0, bytesRead);
	}
	
	//Helper
	
	public static List<ArchivePair> getArchives(File baseDir)
	{
		File archivesFolder = new File(baseDir.getAbsolutePath()+PATH_ADD);
		if(!archivesFolder.exists())
			return null;
		File[] archiveFolders = archivesFolder.listFiles();
		
		List<ArchivePair> pairs = new ArrayList<ArchivePair>();
		for(File archiveFolder : archiveFolders)
		{
			ArchivePair pair = new ArchivePair();
			pair.dir = archiveFolder;
			
			File[] contents = archiveFolder.listFiles();
			for(File file : contents)
			{
				if(file.getName().matches("^Archive_[0-9]+.raf$"))
					pair.raf = file;
				else if(file.getName().matches("^Archive_[0-9]+.raf.dat$"))
					pair.dat = file;
			}
			
			if(pair.raf != null && pair.raf.exists() && pair.dat != null && pair.dat.exists())
				pairs.add(pair);
		}
		
		return pairs;
	}
	
	public static class ArchivePair
	{
		public File dir, raf, dat;
	}
	
	protected static String bytesToHex(byte[] bytes)
	{
		StringBuilder sb = new StringBuilder();
		for(byte b : bytes)
			sb.append(String.format("%02X", b));
		return sb.toString();
	}
}
