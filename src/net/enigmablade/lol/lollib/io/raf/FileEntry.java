package net.enigmablade.lol.lollib.io.raf;

public class FileEntry
{
	public int pathHash, offset, size, pathIndex;
	
	public FileEntry(int pathHash, int offset, int size, int pathIndex)
	{
		this.pathHash = pathHash;
		this.offset = offset;
		this.size = size;
		this.pathIndex = pathIndex;
	}
}
