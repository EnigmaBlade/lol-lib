package net.enigmablade.lol.lollib.io.raf;

import java.io.*;

import static net.enigmablade.lol.lollib.io.raf.RafUtil.*;

public class RafFile
{
	private File file;
	private File parent;
	
	public int version;
	public int managerIndex = -1;
	
	private FileList fileList;
	private PathList pathList;
	
	public RafFile(File file, File parent)
	{
		this.file = file;
		this.parent = parent;
	}
	
	public boolean loadPaths() throws IOException
	{
		if(file == null)
			throw new IllegalStateException("RAF file is null");
		
		RandomAccessFile rafFile = null;
		
		try
		{
			rafFile = new RandomAccessFile(file, "r");
			
			int offset = 0;
			
			////////////
			//Metadata//
			////////////
			
			//Magic number
			byte[] magicNumberB = new byte[4];
			offset += read(rafFile, offset, magicNumberB, true);
			//System.out.println("Magic number: "+bytesToHex(magicNumberB));
			if(!"18BE0EF0".equals(bytesToHex(magicNumberB)))
				throw new IOException("Given file does not pass the magic number check: given \""+bytesToHex(magicNumberB)+"\", expecting \"18BE0EF0\"");
			
			//Version
			byte[] versionB = new byte[4];
			offset += read(rafFile, offset, versionB, true);
			//System.out.println("Version: "+bytesToHex(versionB));
			version = toInt(versionB);
			
			//Manager index
			byte[] managerIndexB = new byte[4];
			offset += read(rafFile, offset, managerIndexB, true);
			//System.out.println("Manager index: "+bytesToHex(managerIndexB));
			managerIndex = toInt(managerIndexB);
			
			//File list offset
			byte[] fileListOffsetB = new byte[4];
			offset += read(rafFile, offset, fileListOffsetB, true);
			int fileListOffset = toInt(fileListOffsetB);
			//System.out.println("File list offset: "+bytesToHex(fileListOffsetB)+" ("+fileListOffset+")");
			
			//Path list offset
			byte[] pathListOffsetB = new byte[4];
			offset += read(rafFile, offset, pathListOffsetB, true);
			int pathListOffset = toInt(pathListOffsetB);
			//System.out.println("Path list offset: "+bytesToHex(pathListOffsetB)+" ("+pathListOffset+")");
			
			/////////////
			//File list//
			/////////////
			
			offset = fileListOffset;
			//System.out.println();
			
			//File list count
			byte[] fileListCountB = new byte[4];
			offset += read(rafFile, offset, fileListCountB, true);
			int fileListCount = toInt(fileListCountB);
			//System.out.println("File list count: "+fileListCount+"\n");
			
			fileList = new FileList(fileListCount);
			
			//File entries
			for(int n = 0; n < fileListCount; n++)
			{
				//Path hash
				byte[] pathHashB = new byte[4];
				offset += read(rafFile, offset, pathHashB, true);
				int pathHash = toInt(pathHashB);
				//System.out.println("\tPath hash: "+pathHash);
				
				//Data offset
				byte[] dataOffsetB = new byte[4];
				offset += read(rafFile, offset, dataOffsetB, true);
				int dataOffset = toInt(dataOffsetB);
				//System.out.println("\tData offset: "+dataOffset);
				
				//Data size
				byte[] dataSizeB = new byte[4];
				offset += read(rafFile, offset, dataSizeB, true);
				int dataSize = toInt(dataSizeB);
				//System.out.println("\tData size: "+dataSize);
				
				//Path list index
				byte[] pathListIndexB = new byte[4];
				offset += read(rafFile, offset, pathListIndexB, true);
				int pathListIndex = toInt(pathListIndexB);
				//System.out.println("\tPath list index: "+pathListIndex+"\n");
				
				fileList.addEntry(pathHash, dataOffset, dataSize, pathListIndex);
			}
			
			/////////////
			//Path list//
			/////////////
			
			offset = pathListOffset;
			
			//Path list size
			byte[] pathListSizeB = new byte[4];
			offset += read(rafFile, offset, pathListSizeB, true);
			//int pathListSize = toInt(pathListSizeB);
			//System.out.println("Path list size: "+pathListSize);
			
			//Path list count
			byte[] pathListCountB = new byte[4];
			offset += read(rafFile, offset, pathListCountB, true);
			int pathListCount = toInt(pathListCountB);
			//System.out.println("Path list count: "+pathListCount);
			
			pathList = new PathList(pathListCount);
			
			//Path entry data
			int[] pathOffsets = new int[pathListCount];
			int[] pathLengths = new int[pathListCount];
			for(int n = 0; n < pathListCount; n++)
			{
				//Path offset
				byte[] pathOffsetB = new byte[4];
				offset += read(rafFile, offset, pathOffsetB, true);
				int pathOffset = toInt(pathOffsetB);
				pathOffsets[n] = pathOffset;
				
				//Path length
				byte[] pathLengthB = new byte[4];
				offset += read(rafFile, offset, pathLengthB, true);
				int pathLength = toInt(pathLengthB);
				pathLengths[n] = pathLength;
			}
			
			//Path entry
			for(int n = 0; n < pathListCount; n++)
			{
				//System.out.println("\tPath offset: "+offset);
				offset = pathListOffset+pathOffsets[n];
				
				//System.out.println("\tPath length: "+pathLengths[n]);
				byte[] pathB = new byte[pathLengths[n]-1];
				
				offset += read(rafFile, offset, pathB, false);
				String path = new String(pathB);
				//System.out.println("\tPath: "+path);
				
				//if(path.matches(pathRegex))
				{
					pathList.addEntry(pathOffsets[n], pathLengths[n], path);
					//System.out.println("\t\tPath matches");
				}
				
				//System.out.println();
			}
			
			//Filter file list
			//fileList.filterFiles(pathList);
			
			return true;
		}
		finally
		{
			if(rafFile != null)
				try
				{
					rafFile.close();
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
		}
	}
	
	//Accessor methods
	
	public File getFile()
	{
		return file;
	}
	
	public File getParent()
	{
		return parent;
	}
	
	public FileList getFileList()
	{
		return fileList;
	}
	
	public PathList getPathList()
	{
		return pathList;
	}
}
