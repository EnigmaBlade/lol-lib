package net.enigmablade.lol.lollib.io.raf;

import java.util.*;

public class FileList extends ArrayList<FileEntry>
{
	public FileList(int length)
	{
		super(length);
	}
	
	public void addEntry(int hash, int offset, int size, int pathIndex)
	{
		add(new FileEntry(hash, offset, size, pathIndex));
	}
	
	public void filterFiles(PathList pathList)
	{
		Iterator<FileEntry> it = iterator();
		while(it.hasNext())
		{
			FileEntry file = it.next();
			boolean remove = true;
			for(PathEntry path : pathList)
				if(file.pathHash == path.pathHash)
				{
					remove = false;
					break;
				}
			if(remove)
				it.remove();
		}
	}
}
