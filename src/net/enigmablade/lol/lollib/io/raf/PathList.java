package net.enigmablade.lol.lollib.io.raf;

import java.util.*;

public class PathList extends ArrayList<PathEntry>
{
	public PathList(int length)
	{
		super(length);
	}
	
	public void addEntry(int offset, int size, String path)
	{
		add(new PathEntry(offset, size, path));
	}
}
