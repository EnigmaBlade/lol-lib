package net.enigmablade.lol.lollib.io.raf;

public class PathEntry implements Comparable<PathEntry>
{
	public int offset, size;
	public String path;
	public int pathHash;
	
	public PathEntry(int offset, int size, String path)
	{
		this.offset = offset;
		this.size = size;
		this.path = path;
		pathHash = pathHash(path);
	}
	
	private static int pathHash(String path)
	{
		int hash = 0;
		int temp = 0;
		for(char c : path.toCharArray())
		{
			hash = (hash << 4) + Character.toLowerCase(c);
			temp = hash & 0xf0000000;
			if(temp != 0)
			{
				hash = hash ^ (temp >> 24);
				hash = hash ^ temp;
			}
		}
		return hash;
	}

	@Override
	public int compareTo(PathEntry e)
	{
		return path.compareTo(e.path);
	}
}
