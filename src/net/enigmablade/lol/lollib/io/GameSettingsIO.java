package net.enigmablade.lol.lollib.io;

import net.enigmablade.paradoxion.io.ini.*;
import static net.enigmablade.paradoxion.util.Logger.*;

public class GameSettingsIO
{
	private static final String configDir = "/Config";
	private static final String configFile = configDir+"/game.cfg";
	
	private INIData data;
	
	private GameSettingsIO(INIData d)
	{
		data = d;
	}
	
	public String getSetting(String group, String key)
	{
		if(hasGroup(group))
			return data.getGroup(group).getValue(key);
		return null;
	}
	
	public boolean setSetting(String group, String key, String value)
	{
		boolean hasGroup = hasGroup(group);
		if(hasGroup)
			data.getGroup(group).setValue(key, value);
		return hasGroup;
	}
	
	public int getIntSetting(String group, String key)
	{
		if(hasGroup(group))
			return data.getGroup(group).getIntValue(key);
		return 0;
	}
	
	public boolean setIntSetting(String group, String key, int value)
	{
		boolean hasGroup = hasGroup(group);
		if(hasGroup)
			data.getGroup(group).setIntValue(key, value);
		return hasGroup;
	}
	
	public double getDoubleSetting(String group, String key)
	{
		if(hasGroup(group))
			return data.getGroup(group).getDoubleValue(key);
		return 0;
	}
	
	public boolean setDoubleSetting(String group, String key, double value)
	{
		boolean hasGroup = hasGroup(group);
		if(hasGroup)
			data.getGroup(group).setDoubleValue(key, value);
		return hasGroup;
	}
	
	public boolean getBooleanSetting(String group, String key)
	{
		if(hasGroup(group))
			return data.getGroup(group).getBooleanValue(key);
		return false;
	}
	
	public boolean setBooleanSetting(String group, String key, boolean value)
	{
		boolean hasGroup = hasGroup(group);
		if(hasGroup)
			data.getGroup(group).setBooleanValue(key, value);
		return hasGroup;
	}
	
	private boolean hasGroup(String group)
	{
		return data != null && data.hasGroup(group);
	}
	
	public boolean saveSettings()
	{
		writeToLog("Saving game settings");
		boolean success = false;
		if(data != null)
			success = INIWriter.writeINI(GamePathUtil.getFile(configFile), data);
		
		if(success)
			writeToLog("Save successful", 1);
		else
			writeToLog("Save failed", 1, LoggingType.ERROR);
		return success;
	}
	
	public static GameSettingsIO loadGameSettings()
	{
		writeToLog("Loading game settings");
		INIData data = INIParser.loadINIFile(GamePathUtil.getFile(configFile));
		if(data != null)
			return new GameSettingsIO(data);
		writeToLog("Failed to load game settings", 1, LoggingType.ERROR);
		return null;
	}
}
