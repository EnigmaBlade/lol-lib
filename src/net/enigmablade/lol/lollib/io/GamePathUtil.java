package net.enigmablade.lol.lollib.io;

import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import net.enigmablade.paradoxion.ui.dialogs.*;
import net.enigmablade.lol.lollib.io.pathhelpers.*;
import net.enigmablade.lol.lollib.io.pathhelpers.platforms.*;
import net.enigmablade.lol.lollib.io.pathhelpers.regions.*;
import net.enigmablade.lol.lollib.ui.dialogs.*;

import static net.enigmablade.paradoxion.util.Logger.*;



public class GamePathUtil
{
	//NA/EU paths
	private static final String GAME_DIR = "RADS/solutions/lol_game_client_sln/releases/${latest}/deploy/";
	private static final String AIR_DIR = "RADS/projects/lol_air_client/releases/${latest}/deploy/";
	
	private static final String IMAGE_DIR = AIR_DIR+"assets/images/";
	private static final String CHARACTER_DIR = "Config/Champions/";
	private static final String SQL_DIR = AIR_DIR+"assets/data/gameStats/";
	private static final String CONFIG_DIR = GAME_DIR+"DATA/CFG/";
	
	private static final String VERSION_PATH = "RADS/projects/lol_air_client/releases/${latest}";
	
	//SEA paths
	private static final String SEA_GAME_DIR = "/Game/";
	private static final String SEA_AIR_DIR = "/Air/";
	
	private static final String SEA_IMAGE_DIR = SEA_AIR_DIR+"assets/images";
	private static final String SEA_CHAR_DIR = SEA_GAME_DIR+"Config/Champions";
	private static final String SEA_SQL_DIR = SEA_AIR_DIR+"assets/data/gameStats/";
	private static final String SEA_CONFIG_DIR = SEA_GAME_DIR+"DATA/CFG/";
	
	//Data
	private static Platform platform;
	private static GamePath baseDir;
	//private static String lolBaseDir;
	private static String gameDir, airDir, characterDir, imageDir, sqlDir, configDir;
	
	//Image loading
	private static Map<String, Image> imageCache;
	
	static
	{
		imageCache = new HashMap<String, Image>();
	}
	
	//Directory finding methods
	
	public static boolean initialize(String path, Region region)
	{
		//Get the current platform
		platform = Platform.getCurrentPlatform();
		
		//Path stuff
		GamePath foundPath = null;
		
		//Check if the path has been found before
		if(path != null && !path.equals(""))
		{
			writeToLog("Path specified in options file: "+path+", region="+region, 1);
			GamePath testPath = new GamePath(path, Platform.WINDOWS, region);
			if(GameFileFilter.isValidDirectory(testPath))
			{
				writeToLog("Path is valid", 1);
				foundPath = testPath;
			}
			else
			{
				writeToLog("Path not valid", 1, LoggingType.WARNING);
			}
		}
		
		//If not given, search for it
		if(foundPath == null)
		{
			PlatformPathHelper pathHelper;
			
			if(platform == Platform.WINDOWS)
			{
				writeToLog("Using Windows platform", 1);
				pathHelper = new WinPathHelper();
			}
			else if(platform == Platform.MACOSX)
			{
				writeToLog("Using Mac OS X platform", 1);
				pathHelper = new MacPathHelper();
			}
			else
			{
				writeToLog("Platform not supported", LoggingType.WARNING);
				return false;
			}
			
			//Find locations
			Set<GamePath> foundPathsSet = pathHelper.findGamePaths();
			GamePath[] foundPaths = foundPathsSet.toArray(new GamePath[0]);
			
			//Handle found locations
			if(foundPaths.length == 1)
			{
				writeToLog("Found a single installation location", 1);
				foundPath = foundPaths[0];
			}
			else if(foundPaths.length > 1)
			{
				writeToLog("Found multiple installation locations", 1);
				String[] paths = new String[foundPaths.length];
				for(int n = 0; n < foundPaths.length; n++)
					paths[n] = foundPaths[n].getPath();
					
				int chosen = MultiSelectionDialog.getDialogInput(null, "Multiple Paths Found", "Select your League of Legends directory:", paths);
				if(chosen > -1)
					foundPath = foundPaths[chosen];
			}
		}
		
		//Couldn't find it, so the user is asked to find it
		if(foundPath == null)
		{
			writeToLog("Could not find the installation path, asking user for path", 1);
			GamePath dir = PathDialog.open(null, platform, null, new GamePath[0]);
			if(dir != null)
			{
				foundPath = dir;
				writeToLog("User input directory: "+foundPath.getPath()+", region="+foundPath.getRegion(), 2);
			}
			else
			{
				writeToLog("User did not input path, closing", 2, LoggingType.ERROR);
				return false;
			}
		}
		
		if(path == null && foundPath.getPath().contains("Program Files"))
			JOptionPane.showMessageDialog(null, "The League of Legends directory is in a Program Files folder.\nPlease make sure you have administrative privileges, otherwise saving may fail.", "Permissions warning", JOptionPane.WARNING_MESSAGE);
		
		return setLoLDir(foundPath, false);
	}
	
	public static boolean setLoLDir(GamePath lolDir, boolean verbose)
	{
		if(!GameFileFilter.isValidDirectory(lolDir))
		{
			if(verbose)
				JOptionPane.showMessageDialog(null, "Invalid directory selected.", "Incorrect Directory", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		baseDir = lolDir;
		writeToLog("Installation location: "+baseDir+", region="+baseDir.getRegion());
		
		gameDir = PathUtil.normalizePath(parsePath(baseDir.getPath()+"/"+GAME_DIR));
		
		airDir = PathUtil.normalizePath(baseDir.getPath())+AIR_DIR;
		
		//Item path
		characterDir = PathUtil.normalizePath(baseDir.getPath());
		if(baseDir.getRegion() == Region.SEA)
			characterDir = characterDir+SEA_CHAR_DIR;
		else
			characterDir = characterDir+CHARACTER_DIR;
		characterDir = PathUtil.normalizePath(characterDir);
		writeToLog("Item path: "+characterDir, 1);
		
		File dir = new File(characterDir);
		if(!dir.exists())
		{
			writeToLog("Character directory doesn't exist, creating...", 1);
			if(!dir.mkdirs())
				writeToLog("Failed to create character directory", 2, LoggingType.ERROR);
		}
		
		//Image path
		imageDir = PathUtil.normalizePath(baseDir.getPath());
		if(baseDir.getRegion() == Region.SEA)
			imageDir += SEA_IMAGE_DIR;
		else
			imageDir = parsePath(imageDir+IMAGE_DIR);
		imageDir = PathUtil.normalizePath(imageDir);
		writeToLog("Image path: "+imageDir, 1);
		
		//SQL database path
		sqlDir = PathUtil.normalizePath(baseDir.getPath());
		if(baseDir.getRegion() == Region.SEA)
			sqlDir += SEA_SQL_DIR;
		else
			sqlDir = parsePath(sqlDir+SQL_DIR);
		sqlDir = PathUtil.normalizePath(sqlDir);
		writeToLog("SQL database path: "+sqlDir, 1);
		
		//Config path
		configDir = PathUtil.normalizePath(baseDir.getPath());
		if(baseDir.getRegion() == Region.SEA)
			configDir += SEA_CONFIG_DIR;
		else
			configDir = parsePath(configDir+CONFIG_DIR);
		configDir = PathUtil.normalizePath(configDir);
		writeToLog("Config path: "+configDir, 1);
		
		return true;
	}
	
	//Path accessor methods
	
	public static GamePath getDir()
	{
		return baseDir;
	}
	
	public static String getGameVersion()
	{
		if(baseDir.getRegion() == Region.SEA)
			return null;
		
		String path = parsePath(PathUtil.normalizePath(baseDir.getPath())+VERSION_PATH);
		int last = Math.max(path.lastIndexOf('\\'), path.lastIndexOf('/'));
		String version = path.substring(last+1);
		return version;
	}
	
	//Utility methods
	
	public static String parsePath(String path)
	{
		int index = path.indexOf('$');
		if(index < 0)
			return path;
		while((index = path.indexOf('$')) > -1)
		{
			String part1 = path.substring(0, index);
			String key = path.substring(index+2, index = path.indexOf('}', index+1));
			String part2 = path.substring(index+1);
			
			String value = "";
			if(key.equals("latest"))
			{
				File file1 = new File(part1);
				String[] files = file1.list();
				if(files != null)
					value = latestVersion(files);
			}
			
			path = part1+value+part2;
		}
				
		return path;
	}
	
	private static String latestVersion(String[] versions)
	{
		int index = 0;
		String newestVersion;
		int[] newestValues;
		do
		{
			newestValues = getVersionValues(newestVersion = versions[index++]);
		}while(newestValues == null && index < versions.length);
		
		for(int i = 1; i < versions.length; i++)
		{
			String version = versions[i];
			int[] versionValues = getVersionValues(version);
			if(versionValues != null)
			{
				for(int n = 0; n < Math.min(newestValues.length, versionValues.length); n++)
				{
					if(versionValues[n] > newestValues[n])
					{
						newestVersion = version;
						newestValues = versionValues;
						break;
					}
					else if(newestValues[n] > versionValues[n])
						break;
				}
			}
		}
		
		return newestVersion;
	}
	
	public static int[] getVersionValues(String s)
	{
		if(s.matches("([\\d]+\\.){3}[\\d]+"))
		{
			String[] parts = s.split("\\.");
			int[] vals = new int[parts.length];
			for(int n = 0; n < parts.length; n++)
				vals[n] = Integer.parseInt(parts[n]);
			return vals;
		}
		return null;
	}
	
	//LoL directory IO methods
	
	public static File getFile(String name)
	{
		return new File(baseDir.getPath()+name);
	}
	
	public static String getGameDir()
	{
		return gameDir;
	}
	
	public static String getAirDir()
	{
		return airDir;
	}
	
	public static String getChampionDir()
	{
		return characterDir;
	}
	
	public static String getConfigDir()
	{
		return configDir;
	}
	
	public static File getSQLDatabase(String name)
	{
		return new File(sqlDir+name);
	}
	
	//LoL image IO
	
	public static ImageIcon getImageIcon(String name)
	{
		if(new File(imageDir+name).exists())
			return new ImageIcon(imageDir+name);
		return null;
	}
	
	public static Image getImage(String name)
	{
		ImageIcon icon = getImageIcon(name);
		if(icon != null)
			return icon.getImage();
		return null;
	}
	
	public static Image getItemImage(String key)
	{
		String cacheKey = "i-"+key;
		Image image = imageCache.get(cacheKey);
		if(image == null)
		{
			image = getImage("items/"+key+".png");
			if(image != null)
				imageCache.put(cacheKey, image);
		}
		if(image == null)
		{
			image = getImage("championScreens/abilities/"+key+".png");
			if(image != null)
				imageCache.put(cacheKey, image);
		}
		return image;
	}
	
	public static Image getChampionImage(String key)
	{
		String cacheKey = "c-"+key;
		Image image = imageCache.get(cacheKey);
		if(image == null)
		{
			image = getImage("champions/"+key+"_Square_0.png");
			//if(image != null)
			//	imageCache.put(cacheKey, image);
		}
		return image;
	}
	
	public static Image getSpellImage(String key)
	{
		String cacheKey = "s-"+key;
		Image image = imageCache.get(cacheKey);
		if(image == null)
		{
			image = getImage("championScreens/abilities/"+key+".png");
			//if(image != null)
			//	imageCache.put(cacheKey, image);
		}
		return image;
	}
	
	public static Image getSummonerSpellImage(String key)
	{
		String cacheKey = "ss-"+key;
		Image image = imageCache.get(cacheKey);
		if(image == null)
		{
			image = getImage("championScreens/abilities/Summoner_"+key+".png");
			//if(image != null)
			//	imageCache.put(cacheKey, image);
		}
		return image;
	}
}
