package net.enigmablade.lol.lollib.io;

import java.io.File;
import javax.swing.filechooser.*;
import net.enigmablade.lol.lollib.io.pathhelpers.*;
import net.enigmablade.lol.lollib.io.pathhelpers.platforms.*;
import net.enigmablade.lol.lollib.io.pathhelpers.regions.*;


public class GameFileFilter extends FileFilter
{
	//private Platform platform;
	
	public GameFileFilter(Platform plat)
	{
		//platform = plat;
	}

	@Override
	public boolean accept(File f)
	{
		if(f.isDirectory())
			return true;
		
		//if(!PlatformPathHelper.getInstanceOf(platform).isValidPath(new GamePath(f.getAbsolutePath(), platform, null)))
		//	return false;
		
		for(Region region : RegionPathHelper.getRegions())
			if(RegionPathHelper.getInstanceOf(region).getExecutableNames().contains(f.getName()))
				return true;
		return false;
	}
	
	@Override
	public String getDescription()
	{
		return "League of Legends Files";
	}
	
	public static boolean isValidDirectory(GamePath path)
	{
		File dir = path.getFile();
		if(!dir.exists() || !dir.isDirectory())
			return false;
		for(String file : dir.list())
			if(RegionPathHelper.getInstanceOf(path.getRegion()).getExecutableNames().contains(file))
				return true;
		return false;
	}
}
