package net.enigmablade.lol.lollib.io;

import java.io.*;
import net.enigmablade.lol.lollib.io.pathhelpers.platforms.*;
import net.enigmablade.lol.lollib.io.pathhelpers.regions.*;


public class GamePath
{
	private String path;
	private Platform platform;
	private Region region;
	
	public GamePath(String p, Platform plat, Region reg)
	{
		path = new File(p).getAbsolutePath();
		platform = plat;
		region = reg;
	}
	
	public String getPath()
	{
		return path;
	}
	
	public File getFile()
	{
		return new File(path);
	}
	
	public Platform getPlatform()
	{
		return platform;
	}
	
	public Region getRegion()
	{
		return region;
	}
	
	@Override
	public String toString()
	{
		return path+" (platform="+platform+", region="+region+")";
	}
	
	@Override
	public int hashCode()
	{
		return path.hashCode()+platform.hashCode()+region.hashCode();
	}
}