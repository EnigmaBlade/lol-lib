package net.enigmablade.lol.lollib.io.pathhelpers.platforms;

import java.io.*;
import java.util.*;
import net.enigmablade.lol.lollib.io.*;
import net.enigmablade.lol.lollib.io.pathhelpers.*;
import net.enigmablade.lol.lollib.io.pathhelpers.regions.*;

import static net.enigmablade.paradoxion.util.Logger.*;

public class WinPathHelper extends PlatformPathHelper
{
	@Override
	public Set<GamePath> findGamePaths()
	{
		//Create list of common root locations
		ArrayList<String> roots = new ArrayList<String>(3);
		File[] drives = File.listRoots();
		for(int i = 0; i < drives.length; i++)
			roots.add(drives[i].getAbsolutePath());
		roots.add(System.getenv("ProgramFiles")+"/");
		roots.add(System.getenv("ProgramFiles(x86)")+"/");
		
		//Search root locations
		Set<GamePath> temp = new LinkedHashSet<GamePath>();
		for(String root : roots)
			temp.addAll(searchRoot(root, false));
		
		//If nothing was round in the root locations, search through all directories in each root (in case the LoL folder has a custom name)
		if(temp.size() == 0)
		{
			writeToLog("No default locations found, repeat and search up a level", 1);
			for(String root : roots)
				temp.addAll(searchRoot(root, true));
		}
		
		return temp;
	}
	
	private static List<GamePath> searchRoot(String root, boolean searchNextLevel)
	{
		root = PathUtil.normalizePath(root);
		
		ArrayList<GamePath> foundLocations = new ArrayList<GamePath>();
		writeToLog("Searching root: "+root, 1);
		
		//Search root location
		for(Region region : RegionPathHelper.getRegions())
		{
			writeToLog("Checking for installations in region "+region, 2);
			RegionPathHelper regionHelper = RegionPathHelper.getInstanceOf(region);
			Set<GamePath> paths = regionHelper.getInstallLocations(root, Platform.WINDOWS);
			for(GamePath path : paths)
				if(GameFileFilter.isValidDirectory(path))
				{
					writeToLog("Valid installation: "+path, 3);
					foundLocations.add(path);
				}
		}
		
		//Search sub-folders
		if(foundLocations.size() == 0 && searchNextLevel)
		{
			File rootFile = new File(root);
			if(rootFile.exists())
			{
				File[] files = new File(root).listFiles();
				for(File f : files)
				{
					if(f.isDirectory())
						foundLocations.addAll(searchRoot(f.getAbsolutePath()+"/", false));
				}
			}
		}
		
		return foundLocations;
	}
	
	@Override
	public boolean isValidPath(GamePath path)
	{
		return true;
	}
}
