package net.enigmablade.lol.lollib.io.pathhelpers.platforms;

public enum Platform
{
	WINDOWS, MACOSX;
	
	public static Platform getCurrentPlatform()
	{
		String osName = System.getProperty("os.name").toLowerCase();
		if(osName.contains("windows"))
			return WINDOWS;
		if(osName.contains("os x"))
			return MACOSX;
		return null;
	}
}
