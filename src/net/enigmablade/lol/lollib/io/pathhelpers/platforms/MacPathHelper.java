package net.enigmablade.lol.lollib.io.pathhelpers.platforms;

import java.io.*;
import java.util.*;
import net.enigmablade.lol.lollib.io.*;
import net.enigmablade.lol.lollib.io.pathhelpers.*;
import net.enigmablade.lol.lollib.io.pathhelpers.regions.*;

//iLoL Open Beta 1.1.app/Contents/Resources/League of Legends.app/Contents/Resources/drive_c/rads/projects/lol_game_client

public class MacPathHelper extends PlatformPathHelper
{
	@Override
	public Set<GamePath> findGamePaths()
	{
		//Applications folder for the system (root of the main drive)
		File appsFolder = new File("/Applications");
		File[] apps = appsFolder.listFiles();
		
		//iLoL is currently only available for the NA/EU-style client, I think, so we don't have to worry about regions
		
		Set<GamePath> temp = new LinkedHashSet<GamePath>();
		
		for(File app : apps)
		{
			GamePath testPath = new GamePath(app.getAbsolutePath(), Platform.MACOSX, Region.NA_EU);
			if(isValidPath(testPath))
				temp.add(testPath);
		}
		
		return temp;
	}
	
	@Override
	public boolean isValidPath(GamePath path)
	{
		return path.getPath().endsWith(".app") && path.getPath().contains("iLoL");
	}
}
