package net.enigmablade.lol.lollib.io.pathhelpers;

import java.util.*;
import net.enigmablade.lol.lollib.io.*;
import net.enigmablade.lol.lollib.io.pathhelpers.platforms.*;


public abstract class PlatformPathHelper
{
	public static Platform[] getPlatforms()
	{
		return Platform.values();
	}
	
	public static PlatformPathHelper getInstanceOf(Platform platform)
	{
		switch(platform)
		{
			case WINDOWS: return new WinPathHelper();
			case MACOSX: return new MacPathHelper();
			
			default: return null;
		}
	}
	
	//Abstract methods
	
	public abstract Set<GamePath> findGamePaths();
	
	public abstract boolean isValidPath(GamePath path);
}
