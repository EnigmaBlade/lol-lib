package net.enigmablade.lol.lollib.io.pathhelpers.regions;

import java.io.*;
import java.util.*;
import net.enigmablade.lol.lollib.io.*;
import net.enigmablade.lol.lollib.io.pathhelpers.*;
import net.enigmablade.lol.lollib.io.pathhelpers.platforms.*;


public class SeaPathHelper extends RegionPathHelper
{
	//Location data
	
	private static final String[] GARENA_LOCATIONS = new String[]{
		"Garena",
		"Garena Messenger",
		"GarenaLoL",
		"GarenaLoLPH",
		"GarenaLoLTW"
	};
	
	private static final String INSTALL_LOCATION_ADD = "/GameData/Apps/";
	
	private static final String[] GAME_LOCATIONS = new String[]{
		"League of Legends",
		"LoL",
		"LoLPH",
		"LoLTW"
	};
	
	//Filter data
	
	private static final String[] EXECUTABLE_NAMES = new String[]{
		"LoLLauncher.exe",
		"lol.exe",
		"lolclient.exe"
	};
	
	//Other
	
	private static boolean searchAll = false;
	
	//Methods
	
	@Override
	public Set<GamePath> getInstallLocations(String basePath, Platform platform)
	{
		Set<GamePath> paths = new HashSet<GamePath>();
		if(!searchAll)
		{
			//Search presets
			for(String garenaLocation : GARENA_LOCATIONS)
			{
				String garenaPath = basePath+garenaLocation;
				if(new File(garenaPath).exists())
				{
					String installPath = garenaPath+INSTALL_LOCATION_ADD;
					for(String gameLocation : GAME_LOCATIONS)
					{
						GamePath path = new GamePath(installPath+gameLocation, platform, Region.SEA);
						paths.add(path);
					}
				}
			}
		}
		else
		{
			//Search all
			File rootDir = new File(basePath);
			File[] garenaDirs = rootDir.listFiles();
			for(File garenaDir : garenaDirs)
			{
				if(garenaDir.exists() && garenaDir.isDirectory() && garenaDir.getName().toLowerCase().startsWith("garena"))
				{
					File appsDir = new File(garenaDir.getAbsoluteFile()+INSTALL_LOCATION_ADD);
					File[] appDirs = appsDir.listFiles();
					for(File appDir : appDirs)
					{
						if(appDir.isDirectory() && appDir.getName().toLowerCase().startsWith("lol"))
						{
							GamePath path = new GamePath(appDir.getAbsolutePath(), platform, Region.SEA);
							paths.add(path);
						}
					}
				}
			}
		}
		return paths;
	}
	
	
	
	@Override
	public Set<String> getExecutableNames()
	{
		Set<String> temp = new HashSet<String>(EXECUTABLE_NAMES.length);
		Collections.addAll(temp, EXECUTABLE_NAMES);
		return temp;
	}
	
	//Other
	
	public static void enableSearchAll(boolean enable)
	{
		searchAll = enable;
	}
}
