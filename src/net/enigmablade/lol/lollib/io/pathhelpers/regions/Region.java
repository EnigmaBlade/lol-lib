package net.enigmablade.lol.lollib.io.pathhelpers.regions;

public enum Region
{
	NA_EU, SEA;
	
	public static Region stringToRegion(String s)
	{
		if("na_eu".equalsIgnoreCase(s))
			return NA_EU;
		else if("sea".equalsIgnoreCase(s))
			return SEA;
		return NA_EU;
	}
}
