package net.enigmablade.lol.lollib.io.pathhelpers.regions;

import java.util.*;
import net.enigmablade.lol.lollib.io.*;
import net.enigmablade.lol.lollib.io.pathhelpers.*;
import net.enigmablade.lol.lollib.io.pathhelpers.platforms.*;


public class NaEuPathHelper extends RegionPathHelper
{
	private static final String[] INSTALL_LOCATIONS = new String[]{
		"Riot Games/League of Legends",
		"League of Legends",
		"League of Legends/League of Legends",
		"LoL",
		
		"Games/Riot Games/League of Legends",
		"Games/League of Legends",
		"Games/League of Legends/League of Legends",
		"Games/LoL"
	};
	
	//Filter data
	
	private static final String[] EXECUTABLE_NAMES = new String[]{
		"lol.launcher.exe",
		"lol.launcher.admin.exe"
	};
	
	//Methods
	
	@Override
	public Set<GamePath> getInstallLocations(String basePath, Platform platform)
	{
		Set<GamePath> paths = new HashSet<GamePath>();
		for(String installLocation : INSTALL_LOCATIONS)
		{
			GamePath path = new GamePath(basePath+installLocation, platform, Region.NA_EU);
			paths.add(path);
		}
		return paths;
	}
	
	@Override
	public Set<String> getExecutableNames()
	{
		Set<String> temp = new HashSet<String>(EXECUTABLE_NAMES.length);
		Collections.addAll(temp, EXECUTABLE_NAMES);
		return temp;
	}
}
