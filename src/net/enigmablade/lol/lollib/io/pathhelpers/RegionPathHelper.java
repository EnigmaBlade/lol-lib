package net.enigmablade.lol.lollib.io.pathhelpers;

import java.util.*;
import net.enigmablade.lol.lollib.io.*;
import net.enigmablade.lol.lollib.io.pathhelpers.platforms.*;
import net.enigmablade.lol.lollib.io.pathhelpers.regions.*;


public abstract class RegionPathHelper
{
	public static Region[] getRegions()
	{
		return Region.values();
	}
	
	public static RegionPathHelper getInstanceOf(Region r)
	{
		switch(r)
		{
			case NA_EU: return new NaEuPathHelper();
			case SEA: return new SeaPathHelper();
			
			default: return new NaEuPathHelper();
		}
	}
	
	//Abstract methods
	
	public abstract Set<GamePath> getInstallLocations(String basePath, Platform platform);
	
	public abstract Set<String> getExecutableNames();
}
