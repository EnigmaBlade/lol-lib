package net.enigmablade.lol.lollib.io.pathhelpers;

import java.io.*;

public class PathUtil
{
	public static String normalizePath(String path)
	{
		File f = new File(path);
		StringBuilder s = new StringBuilder(f.getAbsolutePath());
		if(f.isDirectory())
			s.append(File.separator);
		return s.toString();
	}
}
