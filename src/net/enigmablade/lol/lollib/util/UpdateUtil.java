package net.enigmablade.lol.lollib.util;

import java.io.*;
import java.util.*;
import javax.swing.*;

import net.enigmablade.paradoxion.io.*;
import net.enigmablade.paradoxion.io.cache.*;
import net.enigmablade.paradoxion.localization.*;
import static net.enigmablade.paradoxion.util.Logger.*;

public class UpdateUtil
{
	private static final String cacheURL = "http://enigmablade.net/updates/lol_cache/";
	
	public static void startUpdaterThreaded(final String appKey, final String version, final String buildVersion, final String executable, final boolean verbose)
	{
		
		Thread t = new Thread(new Runnable(){
			@Override
			public void run()
			{
				startUpdater(appKey, version, buildVersion, executable, verbose);
			}
		});
		t.start();
		
	}
	
	public static boolean startUpdater(String appKey, String version, String buildVersion, String executable, boolean verbose)
	{
		try
		{
			writeToLog("Starting updater...");
			String command = "Updater.exe -program "+appKey+" -version "+version+"."+buildVersion+" -exec "+executable+(!verbose ? " -quiet" : "");
			ProcessBuilder builder = new ProcessBuilder(new String[]{"cmd.exe", "/C", command});
			boolean update = false;
			try
			{
				Process process = builder.start();
				InputStream in = process.getInputStream();
				Scanner scanner = new Scanner(in);
				boolean waitForClose = true;
				System.out.println("Before while");
				while(waitForClose && scanner.hasNext())
				{
					String line = scanner.nextLine().trim();
					writeToLog("Updater output: \""+line+"\"", 1);
					if(line.equals("!isUpdating!"))
					{
						update = true;
						waitForClose = false;
					}
					else if(line.equals("!notUpdating!"))
					{
						waitForClose = false;
					}
				}
				System.out.println("After while");
				scanner.close();
			}
			catch(Exception e)
			{
				writeToLog("Error while starting updater", LoggingType.ERROR);
				writeStackTrace(e);
			}
			return update;
		}
		catch(Exception e)
		{
			writeToLog("Failed to start updater", LoggingType.ERROR);
			writeStackTrace(e);
		}
		return false;
	}
	
	public static void finishUpdate(String appKey)
	{
		File dir = new File(System.getProperty("java.io.tmpdir")+"Enigma_Update_"+appKey);
		if(dir.exists() && dir.isDirectory())
		{
			writeToLog("Update not completed, copying remaining files");
			try
			{
				File destDir = new File(new File(".").getCanonicalPath());
				writeToLog("Source dir: "+dir.getAbsolutePath(), 1);
				writeToLog("Dest dir: "+destDir.getAbsolutePath(), 1);
				IOUtil.copyDirectory(dir, destDir);
				IOUtil.deleteDirectory(dir); //I accidentally used destDir when I first wrote this and proceeded to test.  BAD THINGS HAPPENED... ;-;
			}
			catch(IOException e)
			{
				writeToLog("Failed to complete the update", LoggingType.ERROR);
				writeStackTrace(e);
			}
		}
	}
	
	public static boolean startCacheUpdater(final boolean verbose, String... files)
	{
		try
		{
			boolean updated = CacheUpdater.updateCache(new File("cache.dat"), cacheURL, files);
			if(verbose)
			{
				if(updated)
					JOptionPane.showMessageDialog(null, LocaleDatabase.getString("dialog.update.cache.updated"), LocaleDatabase.getString("dialog.update.cache.updatedTitle"), JOptionPane.INFORMATION_MESSAGE);
				else
					JOptionPane.showMessageDialog(null, LocaleDatabase.getString("dialog.update.cache.noUpdate"), LocaleDatabase.getString("dialog.update.cache.noUpdateTitle"), JOptionPane.INFORMATION_MESSAGE);
			}
			return updated;
		}
		catch(Exception e)
		{
			writeToLog("Error while updating cache");
			writeStackTrace(e);
			return false;
		}
	}
}
