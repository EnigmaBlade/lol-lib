package net.enigmablade.lol.lollib.data;

import java.util.*;
import net.enigmablade.lol.lollib.data.db.*;

import static net.enigmablade.paradoxion.util.Logger.*;


public class ItemDatabase
{
	private static boolean sqlLoaded = false, xmlLoaded = false;
	
	public static boolean initDatabase()
	{
		writeToLog("Initializing item databases", 1);
		sqlLoaded = ItemSQLDatabase.initDatabase();
		writeToLog("Item SQL database loaded: "+sqlLoaded, 2);
		if(!sqlLoaded)
		{
			//xmlLoaded = ItemXMLDatabase.initDatabase();
			//writeToLog("Item XML database loaded: "+xmlLoaded, 2);
		}
		return sqlLoaded || xmlLoaded;
	}
	
	public static Item getItem(String id)
	{
		if(sqlLoaded)
			return ItemSQLDatabase.getItem(id);
		//if(xmlLoaded)
		//	return ItemXMLDatabase.getItem(id);
		return null;
	}
	
	public static Set<String> getItems()
	{
		if(sqlLoaded)
			return ItemSQLDatabase.getItems();
		//if(xmlLoaded)
		//	return ItemXMLDatabase.getItems();
		return null;
	}
}
