package net.enigmablade.lol.lollib.data;

import java.util.*;
import net.enigmablade.lol.lollib.data.db.*;

import static net.enigmablade.paradoxion.util.Logger.*;

public class DefaultItemDatabase
{
	private static boolean sqlLoaded = false, xmlLoaded = false;
		
	public static boolean initDatabase()
	{
		writeToLog("Initializing default item databases", 1);
		sqlLoaded = DefaultItemSQLDatabase.initDatabase();
		writeToLog("Default item SQL database loaded: "+sqlLoaded, 2);
		if(!sqlLoaded)
		{
			//xmlLoaded = DefaultItemXMLDatabase.initDatabase();
			//writeToLog("Default item XML database loaded: "+xmlLoaded, 2);
		}
		return sqlLoaded || xmlLoaded;
	}
	
	public static List<String> getDefaultItems(Champion champ, GameMode mode)
	{
		if(sqlLoaded)
			return DefaultItemSQLDatabase.getDefaultItems(champ, mode);
		//if(xmlLoaded)
		//	return DefaultItemXMLDatabase.getDefaultItems(champ, mode);
		return null;
	}
}
