package net.enigmablade.lol.lollib.data;

import static net.enigmablade.paradoxion.util.Logger.*;
import net.enigmablade.lol.lollib.data.db.*;


public class SpellDatabase
{
	private static boolean sqlLoaded = false, xmlLoaded = false;
	
	public static boolean initDatabase()
	{
		writeToLog("Initializing spell databases", 1);
		sqlLoaded = SpellSQLDatabase.initDatabase();
		writeToLog("Spell SQL database loaded: "+sqlLoaded, 2);
		if(!sqlLoaded)
		{
			xmlLoaded = SpellXMLDatabase.initDatabase();
			writeToLog("Spell XML database loaded: "+xmlLoaded, 2);
		}
		return sqlLoaded || xmlLoaded;
	}
	
	public static Spell[] getSpells(Integer id)
	{
		if(sqlLoaded)
			return SpellSQLDatabase.getSpells(id);
		if(xmlLoaded)
			return SpellXMLDatabase.getSpells(id);
		return null;
	}
}
