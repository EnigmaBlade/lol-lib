package net.enigmablade.lol.lollib.data;

import java.util.*;

public class Champion
{
	private Integer id;
	private String key, name, title;
	private Set<ChampionProperty> properties;
	
	private Map<GameMode, List<String>> defaultItems;
	
	public Champion(Integer id, String key, String name, String title)
	{
		this.id = id;
		this.key = key;
		this.name = name;
		this.title = title;
		
		properties = new HashSet<ChampionProperty>();
		defaultItems = new HashMap<GameMode, List<String>>();
	}
	
	public Integer getID()
	{
		return id;
	}
	
	public String getKey()
	{
		return key;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public void addProperty(ChampionProperty property)
	{
		properties.add(property);
	}
	
	public boolean hasProperty(ChampionProperty property)
	{
		return properties.contains(property);
	}
	
	public void addDefaultItems(GameMode mode, List<String> items)
	{
		defaultItems.put(mode, items);
	}
	
	public List<String> getDefaultItems(GameMode mode)
	{
		return defaultItems.get(mode);
	}
	
	@Override
	public String toString()
	{
		return name+" ("+key+")";
	}
}
