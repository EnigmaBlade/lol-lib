package net.enigmablade.lol.lollib.data;

public enum ChampionProperty
{
	//Categories
	ASSASSIN, CARRY, FIGHTER, MAGE, TANK, SUPPORT;
	
	public static ChampionProperty commandToFilter(String com)
	{
		if(com.equals("assassin"))
			return ASSASSIN;
		if(com.equals("carry"))
			return CARRY;
		if(com.equals("fighter"))
			return FIGHTER;
		if(com.equals("mage"))
			return MAGE;
		if(com.equals("tank"))
			return TANK;
		if(com.equals("support"))
			return SUPPORT;
		return null;
	}
}
