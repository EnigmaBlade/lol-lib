package net.enigmablade.lol.lollib.data;

import java.util.*;

public class Item implements Comparable<Item>
{
	private String itemID, itemName, image, tooltip;
	private Map<String, String> stats;
	private int cost, totalCost = -1;
	private Set<ItemProperty> properties;
	private int epicness;
	
	private Set<Item> componentItems;
	private Set<Item> parentItems;
	
	public Item(String id, String name, String img, Map<String, String> st, String tt, int c, int epic)
	{
		itemID = id;
		itemName = name;
		image = img;
		stats = st;
		tooltip = tt;
		cost = c;
		epicness = epic;
		
		componentItems = new HashSet<Item>();
		parentItems = new HashSet<Item>();
		
		properties = new HashSet<ItemProperty>();
		
		initSpecialProperties();
	}
	
	private void initSpecialProperties()
	{
		if("3196".equals(itemID) || "3197".equals(itemID) || "3198".equals(itemID))
			properties.add(ItemProperty.VIKTOR);
		else if("3166".equals(itemID))
			properties.add(ItemProperty.RENGAR);
	}
	
	//Accessor methods
	
	public void addProperty(ItemProperty p)
	{
		properties.add(p);
	}
	
	public boolean hasProperty(ItemProperty p)
	{
		if(properties.contains(p))
			return true;
		return false;
	}
	
	public String getID()
	{
		return itemID;
	}
	
	public String getName()
	{
		return itemName;
	}
	
	public int getCost()
	{
		return cost;
	}
	
	public int getTotalCost()
	{
		if(totalCost < 0)
			return totalCost;
		
		totalCost = cost;
		for (Item component : getComponentItems())
			totalCost += component.getTotalCost();
		return totalCost;
	}
	
	public int getEpicness()
	{
		return epicness;
	}
	
	public Map<String, String> getStats()
	{
		return stats;
	}
	
	public String getToolTip()
	{
		return tooltip;
	}
	
	public String getImage()
	{
		return image;
	}
	
	public void addComponentItem(Item i)
	{
		componentItems.add(i);
	}
	
	public void addParentItem(Item i)
	{
		parentItems.add(i);
	}
	
	public Set<Item> getComponentItems()
	{
		return componentItems;
	}
	
	public Set<Item> getParentItems()
	{
		return parentItems;
	}
	
	//Comparing methods
	
	@Override
	public int compareTo(Item o)
	{
		String name1 = itemName;
		if(name1.startsWith("The "))
			name1 = name1.substring(4);
		String name2 = ((Item)o).getName();
		if(name2.startsWith("The "))
			name2 = name2.substring(4);
		return name1.compareTo(name2);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(!(o instanceof Item))
			return false;
		return itemID.equals(((Item)o).itemID);
	}
	
	@Override
	public int hashCode()
	{
		return itemID.hashCode();
	}
	
	@Override
	public String toString()
	{
		return itemName+" ("+itemID+")";
	}
}
