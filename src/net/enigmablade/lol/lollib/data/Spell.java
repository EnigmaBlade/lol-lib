package net.enigmablade.lol.lollib.data;

public class Spell
{
	private String name, icon;
	
	public Spell(String name, String icon)
	{
		this.name = name;
		this.icon = icon;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getIcon()
	{
		return icon;
	}
	
	@Override
	public String toString()
	{
		return name+" ("+icon+")";
	}
}
