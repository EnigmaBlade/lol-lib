package net.enigmablade.lol.lollib.data;

import java.util.*;
import net.enigmablade.lol.lollib.data.db.*;

import static net.enigmablade.paradoxion.util.Logger.*;

public class ChampionDatabase
{
	private static boolean sqlLoaded = false, xmlLoaded = false;
	
	public static boolean initDatabase()
	{
		writeToLog("Initializing champion databases", 1);
		sqlLoaded = ChampionSQLDatabase.initDatabase();
		writeToLog("Champion SQL database loaded: "+sqlLoaded, 2);
		if(!sqlLoaded)
		{
			//xmlLoaded = ChampionXMLDatabase.initDatabase();
			//writeToLog("Champion XML database loaded: "+xmlLoaded, 2);
		}
		return sqlLoaded || xmlLoaded;
	}
	
	public static Champion getChampion(String key)
	{
		if(sqlLoaded)
			return ChampionSQLDatabase.getChampion(key);
		//if(xmlLoaded)
		//	return ChampionXMLDatabase.getChampion(key);
		return null;
	}
	
	public static Champion getChampion(int id)
	{
		if(sqlLoaded)
			return ChampionSQLDatabase.getChampion(id);
		//if(xmlLoaded)
		//	return ChampionXMLDatabase.getChampion(key);
		return null;
	}
	
	public static String getChampionKey(String name)
	{
		if(sqlLoaded)
			return ChampionSQLDatabase.getChampionKey(name);
		//if(xmlLoaded)
		//	return ChampionXMLDatabase.getChampionKey(name);
		return null;
	}
	
	public static Set<String> getChampions()
	{
		if(sqlLoaded)
			return ChampionSQLDatabase.getChampions();
		//if(xmlLoaded)
		//	return ChampionXMLDatabase.getChampions();
		return null;
	}
}
