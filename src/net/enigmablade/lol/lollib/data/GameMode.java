package net.enigmablade.lol.lollib.data;

public enum GameMode
{
	ANY("ANY", "Any", "Any Map", "any", "any"),
	CLASSIC("CLASSIC", "Classic 5v5", "Summoner's Rift", "1", "CLASSIC"),
	TWISTED("CLASSICMAP10", "Classic 3v3", "The Twisted Treeline", "10", "CLASSIC"),
	DOMINION("DOMINION", "Dominion", "The Crystal Scar", "8", "ODIN"),
	//ALL_MID("ARAM", "All-mid", "The Proving Grounds", "3", "ARAM");
	ALL_MID("ARAM", "All-mid", "Howling Abyss", "12", "ARAM");
	
	private String key;
	private String title;
	private String details;
	
	private String mapID;
	private String modeID;
	
	private GameMode(String k, String t, String d, String maID, String moID)
	{
		key = k;
		title = t;
		details = d;
		mapID = maID;
		modeID = moID;
	}
	
	public ItemProperty getItemProperty()
	{
		switch(this)
		{
			case ANY: return ItemProperty.ANY_MODE;
			case CLASSIC: return ItemProperty.CLASSIC_MODE;
			case TWISTED: return ItemProperty.TWISTED_MODE;
			case DOMINION: return ItemProperty.DOMINION_MODE;
			case ALL_MID: return ItemProperty.ALL_MID_MODE;
			
			default: return null;
		}
	}
	
	public String getKey()
	{
		return key;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public String getDetails()
	{
		return details;
	}
	
	public String getMapID()
	{
		return mapID;
	}
	
	public String getModeID()
	{
		return modeID;
	}
	
	public static GameMode stringToMode(String text)
	{
		text = text.toLowerCase();
		if(text.equals("any"))
			return ANY;
		if(text.equals("classic"))
			return CLASSIC;
		if(text.equals("twisted"))
			return TWISTED;
		if(text.equals("dominion"))
			return DOMINION;
		if(text.equals("allmid"))
			return ALL_MID;
		return CLASSIC;
	}
}
