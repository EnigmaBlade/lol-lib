package net.enigmablade.lol.lollib.data.db;

import java.sql.*;
import java.util.*;
import net.enigmablade.lol.lollib.data.*;

import static net.enigmablade.paradoxion.util.Logger.*;


public class SpellSQLDatabase
{
	private static HashMap<Integer, Spell[]> spells;
	
	public static boolean initDatabase()
	{
		spells = new HashMap<Integer, Spell[]>();
		
		Connection conn = SQLDatabaseUtil.getSQLDatabase();
		if(conn != null)
		{
			try
			{
				Statement stat = conn.createStatement();
				
				ResultSet table = stat.executeQuery("SELECT name, iconPath, championId, rank FROM championAbilities WHERE rank > 1;");
				while(table.next())
				{
					String name = table.getString("name");
					String icon = table.getString("iconPath");
					if(icon != null)
						icon = icon.substring(0, Math.max(icon.lastIndexOf('.'), 0));
					
					Spell spell = new Spell(name, icon);
					
					Integer championID = table.getInt("championId");
					Spell[] newSpells = spells.get(championID);
					if(newSpells == null)
						newSpells = new Spell[4];
					
					int index = table.getInt("rank")-2;
					newSpells[index] = spell;
					
					spells.put(championID, newSpells);
				}
				
				table.close();
				
				return true;
			}
			catch(Exception e)
			{
				writeToLog("Failed to load spell database", LoggingType.ERROR);
				writeStackTrace(e);
			}
			finally
			{
				try
				{
					conn.close();
				}
				catch(SQLException e){}
			}
		}
		
		return false;
	}
	
	public static Spell[] getSpells(Integer championID)
	{
		if(spells != null)
			return spells.get(championID);
		return null;
	}
}
