package net.enigmablade.lol.lollib.data.db;

import java.util.*;

import net.enigmablade.paradoxion.io.cache.*;
import net.enigmablade.paradoxion.io.xml.*;
import static net.enigmablade.paradoxion.util.Logger.*;

import net.enigmablade.lol.lollib.data.*;

public class DefaultItemXMLDatabase
{
	private static int databaseVersion;
	private static boolean loaded = false;
	private static Map<String, List<String>> normalItems, dominionItems, allMidItems;
		
	public static boolean initDatabase()
	{
		normalItems = new HashMap<String, List<String>>();
		dominionItems = new HashMap<String, List<String>>();
		allMidItems = new HashMap<String, List<String>>();
		
		try
		{
			XMLParser parser = new XMLParser(CacheResourceLoader.getResourceStream("default_items.xml"), "xml", "1.0");
			
			XMLNode root = parser.getRoot();
			databaseVersion = root.getIntAttribute("version");
			
			ArrayList<XMLNode> children = root.getChildren();
			for(XMLNode championNode : children)
			{
				if(championNode.getName().equals("champion"))
				{
					String champKey = championNode.getAttribute("key");
					
					ArrayList<XMLNode> modeNodes = championNode.getChildren();
					for(XMLNode modeNode : modeNodes)
					{
						if(modeNode.getName().equals("mode"))
						{
							GameMode modeType = GameMode.stringToMode(modeNode.getAttribute("value"));
							ArrayList<XMLNode> itemNodes = modeNode.getChildren();
							List<String> items = new ArrayList<String>(6);
							for(XMLNode itemNode : itemNodes)
							{
								String itemID = itemNode.getAttribute("id");
								items.add(itemID);
							}
							
							getDatabase(modeType).put(champKey, items);
						}
					}
				}
			}
			
			loaded = true;
		}
		catch(Exception e)
		{
			writeToLog("Error: Failed to load item database", LoggingType.ERROR);
			writeStackTrace(e);
			return false;
		}
		
		return true;
	}
	
	public static List<String> getDefaultItems(Champion champ, GameMode mode)
	{
		return loaded ? getDatabase(mode).get(champ.getKey()) : null;
	}
	
	public static int getDatabaseVersion()
	{
		return loaded ? databaseVersion : -1;
	}
	
	private static Map<String, List<String>> getDatabase(GameMode mode)
	{
		switch(mode)
		{
			case CLASSIC: return normalItems;
			case DOMINION: return dominionItems;
			case ALL_MID: return allMidItems;
			
			default: return null;
		}
	}
}
