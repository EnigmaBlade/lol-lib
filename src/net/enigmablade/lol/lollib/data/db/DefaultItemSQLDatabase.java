package net.enigmablade.lol.lollib.data.db;

import java.sql.*;
import java.util.*;
import net.enigmablade.lol.lollib.data.*;

import static net.enigmablade.paradoxion.util.Logger.*;


public class DefaultItemSQLDatabase
{
	private static Connection database;
	private static Statement statement;
	
	public static boolean initDatabase()
	{
		database = SQLDatabaseUtil.getSQLDatabase();
		if(database != null)
		{
			try
			{
				statement = database.createStatement();
			}
			catch(Exception e)
			{
				writeToLog("Failed to load default item database", LoggingType.ERROR);
				writeStackTrace(e);
			}
			
			return true;
		}
		else
		{
			writeToLog("Dababase file does not exist, is LoL installed?", LoggingType.ERROR);
		}
		
		return false;
	}
	
	public static List<String> getDefaultItems(Champion champ, GameMode mode)
	{
		ResultSet table = null;
		try
		{
			String query = "SELECT itemId FROM championItems WHERE championId == "+champ.getID()+" AND gameMode == \""+mode.getKey()+"\" ORDER BY itemId ASC;";
			table = statement.executeQuery(query);
			ArrayList<String> items = new ArrayList<String>(6);
			while(table.next())
				items.add(table.getString("itemId"));
			return items;
		}
		catch(Exception e)
		{
			writeToLog("Failed to get default items from database", LoggingType.ERROR);
			writeStackTrace(e);
		}
		finally
		{
			if(table != null)
				try
				{
					table.close();
				}
				catch(SQLException e){}
		}
		return null;
	}
	
	public static void cleanup()
	{
		try
		{
			statement.close();
			database.close();
		}
		catch(SQLException e)
		{
			writeStackTrace(e);
		}
	}
}
