package net.enigmablade.lol.lollib.data.db;

import java.util.*;

import net.enigmablade.paradoxion.io.cache.*;
import net.enigmablade.paradoxion.io.xml.*;
import static net.enigmablade.paradoxion.util.Logger.*;

import net.enigmablade.lol.lollib.data.*;

public class ChampionXMLDatabase
{
	private static int databaseVersion;
	
	private static HashMap<String, Champion> champions;
	private static HashMap<String, String> nameToKey;
		
	public static boolean initDatabase()
	{
		champions = new HashMap<String, Champion>();
		nameToKey = new HashMap<String, String>();
		
		try
		{
			XMLParser parser = new XMLParser(CacheResourceLoader.getResourceStream("champions.xml"), "xml", "1.0");
			
			XMLNode root = parser.getRoot();
			databaseVersion = root.getIntAttribute("version");
			
			ArrayList<XMLNode> children = root.getChildren();
			for(XMLNode child : children)
			{
				if(child.getName().equals("champion"))
				{
					String key = child.getAttribute("key");
					String name = child.getAttribute("name");
					Champion champion = new Champion(-1, key, name, "");
					
					XMLNode sortingNode = child.getChild("sorting");
					String propertiesString = sortingNode.getAttribute("value");
					String[] properties = propertiesString.split(",");
					for(String s : properties)
					{
						ChampionProperty property = ChampionProperty.commandToFilter(s);
						if(property != null)
							champion.addProperty(property);
						else
							writeToLog("Null property on champion "+name+": "+s, 1, LoggingType.WARNING);
					}
					
					champions.put(key, champion);
					nameToKey.put(name, key);
				}
			}
		}
		catch(Exception e)
		{
			writeToLog("Failed to load champion database", LoggingType.ERROR);
			writeStackTrace(e);
			return false;
		}
		
		return true;
	}
	
	public static Champion getChampion(String key)
	{
		if(champions != null)
			return champions.get(key);
		return null;
	}
	
	public static String getChampionKey(String name)
	{
		if(nameToKey != null)
			return nameToKey.get(name);
		return null;
	}
	
	public static Set<String> getChampions()
	{
		if(champions != null)
			return champions.keySet();
		return null;
	}
	
	public static int getDatabaseVersion()
	{
		return databaseVersion;
	}
}
