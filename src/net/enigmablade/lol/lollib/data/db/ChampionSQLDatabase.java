package net.enigmablade.lol.lollib.data.db;

import java.sql.*;
import java.util.*;
import net.enigmablade.lol.lollib.data.*;

import static net.enigmablade.paradoxion.util.Logger.*;


public class ChampionSQLDatabase
{
	private static HashMap<String, Champion> champions;
	private static HashMap<Integer, Champion> championIds;
	private static HashMap<String, String> nameToKey;
	
	public static boolean initDatabase()
	{
		champions = new HashMap<String, Champion>();
		championIds = new HashMap<Integer, Champion>();
		nameToKey = new HashMap<String, String>();
		
		Connection database = SQLDatabaseUtil.getSQLDatabase();
		if(database != null)
		{
			try
			{
				Statement stat = database.createStatement();
				
				ResultSet table = stat.executeQuery("SELECT * FROM champions;");
				while(table.next())
				{
					Integer id = table.getInt("id");
					String key = table.getString("name");
					String name = table.getString("displayName");
					String title = table.getString("title");
					String tags = table.getString("tags");
					
					Champion champion = new Champion(id, key, name, title);
					
					String[] tagsList = tags.split(",");
					for(String tag : tagsList)
					{
						ChampionProperty property = ChampionProperty.commandToFilter(tag);
						if(property != null)
							champion.addProperty(property);
					}
					
					champions.put(key, champion);
					championIds.put(id, champion);
					nameToKey.put(name, key);
				}
				
				table.close();
				database.close();
				
				return true;
			}
			catch(Exception e)
			{
				writeToLog("Failed to load champion database", LoggingType.ERROR);
				writeStackTrace(e);
			}
			finally
			{
				try
				{
					database.close();
				}
				catch(SQLException e){}
			}
		}
		else
		{
			writeToLog("Dababase file does not exist, is LoL installed?", LoggingType.ERROR);
		}
		
		return false;
	}
	
	public static Champion getChampion(String key)
	{
		if(champions != null)
			return champions.get(key);
		return null;
	}
	
	public static Champion getChampion(int id)
	{
		if(championIds != null)
			return championIds.get(id);
		return null;
	}
	
	public static String getChampionKey(String name)
	{
		if(nameToKey != null)
			return nameToKey.get(name);
		return null;
	}
	
	public static Set<String> getChampions()
	{
		if(champions != null)
			return champions.keySet();
		return null;
	}
}
