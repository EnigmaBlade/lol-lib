package net.enigmablade.lol.lollib.data.db;

import java.io.*;
import java.sql.*;
import java.util.*;

import net.enigmablade.paradoxion.io.cache.*;
import net.enigmablade.paradoxion.io.xml.*;
import static net.enigmablade.paradoxion.util.Logger.*;

import net.enigmablade.lol.lollib.data.*;

public class ItemSQLDatabase
{
	private static HashMap<String, Item> items;
	
	public static boolean initDatabase()
	{
		items = new HashMap<String, Item>();
		
		writeToLog("ItemSQLDatabase # Opening connection to database");
		Connection database = SQLDatabaseUtil.getSQLDatabase();
		if(database != null)
		{
			//Load items
			writeToLog("ItemSQLDatabase # Loading items", 1);
			try
			{
				Statement table = null;
				try
				{
					table = database.createStatement();
					
					
					ResultSet result = table.executeQuery("SELECT id, name, description, iconPath, price, epicness FROM items;");
					while(result.next())
					{
						String id = result.getString("id");
						String name = result.getString("name");
						String tooltip = result.getString("description");
						String image = result.getString("iconPath");
						if(image != null)
							image = image.substring(0, Math.max(image.lastIndexOf('.'), 0));
						Integer cost = result.getInt("price");
						Integer epicness = result.getInt("epicness");
						
						//Parse tooltip
							
						Map<String, String> stats = new HashMap<String, String>();
						try
						{
							//Stats
							int startIndex;
							if((startIndex = tooltip.indexOf("<stats>")) > -1)
							{
								int endIndex = tooltip.indexOf("</stats>");
								//Get stats
								String statsString = tooltip.substring(startIndex+7, endIndex);
								
								stats = extractStats(statsString);
								//Remove stats from tooltip
								String before = tooltip.substring(0, startIndex);
								String after = tooltip.substring(endIndex+8);
								tooltip = before+after;
							}
							//Effects
							tooltip = tooltip.replaceAll("unique", "b");
							tooltip = tooltip.replaceAll("passive", "b");
							tooltip = tooltip.replaceAll("active", "b");
							tooltip = tooltip.replaceAll("consumable", "b");
							tooltip = tooltip.replaceAll("aura", "b");
							
							//Break lines to size
							ArrayList<String> lines = breakLines(tooltip);
							if(lines.size() > 0)
							{
								tooltip = lines.get(0);
								for(int i = 1 ; i < lines.size(); i++)
								{
									tooltip += "\n"+lines.get(i);
								}
							}
							
							//Clean up
							tooltip = cleanBreaks(tooltip);
						}
						catch(Exception e)
						{
							writeToLog("ItemSQLDatabase # Failed to properly parse tooltip and stats", 2, LoggingType.ERROR);
							writeStackTrace(e);
							
							tooltip = "<i>An error occured when parsing tooltip.</i>";
						}
						
						tooltip = "<html>"+tooltip+"</html>";
						
						//Create item
						Item item = new Item(id, name, image, stats, tooltip, cost, epicness);
						items.put(id, item);
					}
					
					result.close();
				}
				finally
				{
					if(table != null)
						table.close();
				}
				
				//Load categories
				writeToLog("ItemSQLDatabase # Loading item categories", 1);
				for(String key : items.keySet())
				{
					Item item = items.get(key);
					
					Statement catTable = null;
					try
					{
						catTable = database.createStatement();
						
						ResultSet result = catTable.executeQuery("SELECT itemCategoryId FROM itemItemCategories WHERE itemId=="+key+";");
						while(result.next())
						{
							String sortID = result.getString("itemCategoryId");
							ItemProperty sortProperty = ItemProperty.getFilterProperty(sortID);
							item.addProperty(sortProperty);
						}
						result.close();
					}
					finally
					{
						if(catTable != null)
							catTable.close();
					}
				}
				
				//Load recipes
				writeToLog("ItemSQLDatabase # Loading item recipes", 1);
				for(String key : items.keySet())
				{
					Item parent = items.get(key);
					
					Statement recipeTable = null;
					try
					{
						recipeTable = database.createStatement();
						
						ResultSet result = recipeTable.executeQuery("SELECT recipeItemId FROM itemRecipes WHERE buildsToItemId=="+key+";");
						while(result.next())
						{
							Item component = items.get(result.getString("recipeItemId"));
							if(component != null)
							{
								parent.addComponentItem(component);
								component.addParentItem(parent);
							}
						}
						result.close();
					}
					finally
					{
						if(recipeTable != null)
							recipeTable.close();
					}
				}
			}
			catch(SQLException e)
			{
				writeToLog("ItemSQLDatabase # Failed to item information from database", 1, LoggingType.ERROR);
				writeStackTrace(e);
				return false;
			}
			finally
			{
				try
				{
					database.close();
				}
				catch(SQLException e){}
			}
			
			//Load mode data
				//Load from cache file
			writeToLog("ItemSQLDatabase # Loading item mode data from cache", 1);
			InputStream in = null;
			try
			{
				File local = new File("../cache/items.xml");
				local = new File(local.getCanonicalPath());
				if(local.exists())
					in = new FileInputStream(local);
				if(in == null)
				{
					writeToLog("ItemSQLDatabase # Local items.xml not found, loading default", 1, LoggingType.WARNING);
					in = CacheResourceLoader.getResourceStream("items.xml");
				}
				XmlParser parser = new XmlParser(in, "xml", "1.0");
				XmlNode root = parser.getRoot();
				List<XmlNode> children = root.getChildren();
				for(XmlNode child : children)
				{
					if("mode".equals(child.getName()))
					{
						String type = child.getAttribute("type");
						
						List<XmlNode> itemNodes = child.getChildren();
						for(XmlNode itemNode : itemNodes)
						{
							if("item".equals(itemNode.getName()))
							{
								String id = itemNode.getAttribute("id");
								Item item = items.get(id);
								if(item != null)
								{
									switch(type.hashCode())
									{
										case 0xE5F: //Summoner's Rift
											item.addProperty(ItemProperty.CLASSIC_MODE);
											break;
										case 0xE80: //Twisted Treeline
											item.addProperty(ItemProperty.TWISTED_MODE);
											break;
										case 0x18542: //Dominion
											item.addProperty(ItemProperty.DOMINION_MODE);
											break;
										case 0xDF7: //Proving Grounds
											item.addProperty(ItemProperty.ALL_MID_MODE);
											break;
										case 0xAF3F29EB: //Common
											item.addProperty(ItemProperty.CLASSIC_MODE);
											item.addProperty(ItemProperty.TWISTED_MODE);
											item.addProperty(ItemProperty.DOMINION_MODE);
											item.addProperty(ItemProperty.ALL_MID_MODE);
											break;
									}
								}
								else
									writeToLog("Item does not exist, id="+id, 2, LoggingType.WARNING);
							}
						}
					}
					else if("property".equals(child.getName()))
					{
						String property = child.getAttribute("value");
						
						List<XmlNode> itemNodes = child.getChildren();
						for(XmlNode itemNode : itemNodes)
						{
							if("item".equals(itemNode.getName()))
							{
								String id = itemNode.getAttribute("id");
								Item item = items.get(id);
								if(item != null)
									item.addProperty(ItemProperty.getFilterProperty(property));
								else
									writeToLog("Item does not exist, id="+id, 2, LoggingType.WARNING);
							}
						}
					}
				}
			}
			catch(Exception e)
			{
				writeToLog("ItemSQLDatabase # Failed to load item mode data", 1, LoggingType.ERROR);
				writeStackTrace(e);
			}
			finally
			{
				if(in != null)
					try
					{
						in.close();
					}
					catch(IOException e){}
			}
				//Set each item to be shown in any
			for(String key : items.keySet())
			{
				Item item = items.get(key);
				item.addProperty(ItemProperty.ANY_MODE);
				//item.addProperty(ItemProperty.CLASSIC_MODE);
				//item.addProperty(ItemProperty.TWISTED_MODE);
				//item.addProperty(ItemProperty.DOMINION_MODE);
				//item.addProperty(ItemProperty.ALL_MID_MODE);
			}
			
			return true;
		}
		else
		{
			writeToLog("ItemSQLDatabase # Dababase file does not exist, is LoL installed?", 1, LoggingType.ERROR);
		}
		
		return false;
	}
	
	private static Map<String, String> extractStats(String statsString)
	{
		Map<String, String> statsMap = new HashMap<String, String>();
		if(statsString.contains("<unique>"))
			statsMap.put(" ", "There is a typo in the database! D:");
		else
		{
			String[] parts = statsString.split("<br>");
			for(String statString : parts)
			{
				statString = cleanBreaks(statString);
				int mid = statString.indexOf(' ');
				String value = statString.substring(0, mid).trim();
				String key = statString.substring(mid+1).trim();
				statsMap.put(key, value);
			}
		}
		return statsMap;
	}
	
	private static ArrayList<String> breakLines(String lineToBreak)
	{
		if(lineToBreak.length() <= 56)
		{
			ArrayList<String> minimumLines = new ArrayList<String>();
			minimumLines.add(lineToBreak);
			return minimumLines;
		}
		else
		{
			int wordIndex = 55;
			while(lineToBreak.charAt(wordIndex) != ' ')
				wordIndex--;
			ArrayList<String> theLines = breakLines(lineToBreak.substring(0, wordIndex));
			theLines.addAll(breakLines(lineToBreak.substring(wordIndex)));
			return theLines;
		}
	}
	
	private static String cleanBreaks(String s)
	{
		s = s.trim();
		String temp = s.toLowerCase();
		while(temp.startsWith("<br>"))
		{
			s = s.substring(4).trim();
			temp = s.toLowerCase();
		}
		while(temp.endsWith("<br>"))
		{
			s = s.substring(0, s.length()-4).trim();
			temp = s.toLowerCase();
		}
		return s;
	}
	
	//Accessor methods
	
	public static Item getItem(String id)
	{
		return items.get(id);
	}
	
	public static Set<String> getItems()
	{
		return items.keySet();
	}
}
