package net.enigmablade.lol.lollib.data.db;

import java.util.*;

import net.enigmablade.paradoxion.io.cache.*;
import net.enigmablade.paradoxion.io.xml.*;
import static net.enigmablade.paradoxion.util.Logger.*;

import net.enigmablade.lol.lollib.data.*;

public class ItemXMLDatabase
{
	private static int databaseVersion;
	
	private static HashMap<String, Item> items;
	
	public static boolean initDatabase()
	{
		items = new HashMap<String, Item>();
		
		try
		{
			XMLParser parser = new XMLParser(CacheResourceLoader.getResourceStream("items.xml"), "xml", "1.0");
			
			XMLNode root = parser.getRoot();
			databaseVersion = root.getIntAttribute("version");
			
			Map<Item, Set<String>> itemComponents = new HashMap<Item, Set<String>>();
			
			ArrayList<XMLNode> children = root.getChildren();
			for(XMLNode child : children)
			{
				if("item".equals(child.getName()))
				{
					String id = child.getAttribute("id");
					String name = child.getAttribute("name");
					String image = child.getChild("image").getAttribute("value");
					String[] modes = child.getChild("mode").getAttribute("value").split(",");
					String[] sortValues = child.getChild("sorting").getAttribute("value").split(",");
					int cost = child.getChild("cost").getIntAttribute("value");
					
					Map<String, String> stats = new TreeMap<String, String>();
					XMLNode statsNode = child.getChild("stats");
					ArrayList<XMLNode> statNodes = statsNode.getChildren();
					for(XMLNode statNode : statNodes)
						stats.put(statNode.getAttribute("type"), statNode.getAttribute("value"));
					
					XMLNode componentsNode = child.getChild("components");
					Set<String> components = new HashSet<String>();
					if(componentsNode != null)
					{
						for(XMLNode componentNode : componentsNode.getChildren())
							components.add(componentNode.getAttribute("value"));
					}
					
					String tooltip = child.getChild("tooltip").getAttribute("value");
					if(stats.containsKey("ten"))
						tooltip += (tooltip.length() > 0 ? "\n\n" : "")+"(${TEN} reduces the duration of stuns, slows, taunts, fears, sleeps, silences, blinds and immobilizes. ${TEN} effects do not stack)";
					tooltip = tooltip.replace("\\n", "\n");
					tooltip = tooltip.replace("\t", "    ");
					
					XMLNode ttStatsNode = child.getChild("tooltipstats");
					int index = -1;
					while((index = tooltip.indexOf('$')) >= 0)
					{
						String part1 = tooltip.substring(0, index);
						String statKey = tooltip.substring(tooltip.indexOf('{', index)+1, index = tooltip.indexOf('}', index));
						String part2 = tooltip.substring(index+1);
						
						String replacement = XMLDatabaseUtil.parseReplacement(statKey);
						if(replacement == null)
						{
							if(ttStatsNode == null)
								writeToLog("Missing stats node on item "+name+", can't get stat "+statKey, LoggingType.WARNING);
							replacement = ttStatsNode.getAttribute(statKey);
							if(replacement == null)
								writeToLog("Null attribute on item "+name+": "+statKey, LoggingType.WARNING);
						}
						tooltip = part1+replacement+part2;
					}
					
					String tooltipFixed = tooltip;
					String[] tooltipLines = tooltipFixed.split("\n");
					ArrayList<String> theLines = new ArrayList<String>();
					for(String tip : tooltipLines)
						theLines.addAll(breakLines(tip));
					tooltipLines = theLines.toArray(tooltipLines);
					if(tooltipLines.length > 0)
					{
						tooltipFixed = tooltipLines[0];
						for(int i = 1 ; i < tooltipLines.length; i++)
						{
							tooltipFixed += "\n"+tooltipLines[i];
						}
					}
					
					Item item = new Item(id, name, image, modes, sortValues, stats, tooltip, tooltipFixed, cost);
					items.put(id, item);
					itemComponents.put(item, components);
				}
			}
			
			for(Item item : itemComponents.keySet())
			{
				Set<String> components = itemComponents.get(item);
				for(String s : components)
				{
					Item component = items.get(s);
					if(component != null)
					{
						item.addComponentItem(component);
						component.addParentItem(item);
					}
				}
			}
		}
		catch(Exception e)
		{
			writeToLog("Failed to load item database", LoggingType.ERROR);
			writeStackTrace(e);
			return false;
		}
		
		return true;
	}
	
	private static ArrayList<String> breakLines(String lineToBreak)
	{
		if(lineToBreak.length() <= 56)
		{
			ArrayList<String> minimumLines = new ArrayList<String>();
			minimumLines.add(lineToBreak);
			return minimumLines;
		}
		else
		{
			int wordIndex = 55;
			while(lineToBreak.charAt(wordIndex) != ' ')
				wordIndex--;
			ArrayList<String> theLines = breakLines(lineToBreak.substring(0, wordIndex));
			theLines.addAll(breakLines(lineToBreak.substring(wordIndex)));
			return theLines;
		}
	}
	
	public static Item getItem(String id)
	{
		return items.get(id);
	}
	
	public static Set<String> getItems()
	{
		return items.keySet();
	}
	
	public static int getDatabaseVersion()
	{
		return databaseVersion;
	}
}
