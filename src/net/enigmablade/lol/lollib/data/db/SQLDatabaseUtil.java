package net.enigmablade.lol.lollib.data.db;

import java.io.*;
import java.sql.*;
import net.enigmablade.lol.lollib.io.*;

import static net.enigmablade.paradoxion.util.Logger.*;


public class SQLDatabaseUtil
{
	protected static Connection getSQLDatabase()
	{
		File dbFile = GamePathUtil.getSQLDatabase("gameStats_en_US.sqlite");
		if(!dbFile.exists())
			dbFile = GamePathUtil.getSQLDatabase("gameStats_en_GB.sqlite");
		if(dbFile.exists())
		{
			try
			{
				Class.forName("org.sqlite.JDBC");
				Connection conn = DriverManager.getConnection("jdbc:sqlite:"+dbFile.getAbsolutePath());
				return conn;
			}
			catch(Exception e)
			{
				writeToLog("Error when opening database", LoggingType.ERROR);
				writeStackTrace(e);
			}
		}
		else
		{
			writeToLog("Dababase file does not exist, is LoL installed?", LoggingType.ERROR);
		}
		return null;
	}
}
