package net.enigmablade.lol.lollib.data.db;

import java.util.*;
import net.enigmablade.paradoxion.io.cache.*;
import net.enigmablade.paradoxion.io.xml.*;
import net.enigmablade.lol.lollib.data.*;

import static net.enigmablade.paradoxion.util.Logger.*;


public class SpellXMLDatabase
{
	private static int databaseVersion;
	
	private static HashMap<Integer, Spell[]> spells;
		
	public static boolean initDatabase()
	{
		spells = new HashMap<Integer, Spell[]>();
		
		try
		{
			XmlParser parser = new XmlParser(CacheResourceLoader.getResourceStream("spells.xml"), "xml", "1.0");
			
			XmlNode root = parser.getRoot();
			databaseVersion = root.getIntAttribute("version");
			
			ArrayList<XmlNode> championNodes = root.getChildren();
			for(XmlNode championNode : championNodes)
			{
				if(championNode.getName().equals("champion"))
				{
					Integer champID = -1;
					String champKey = championNode.getAttribute("key");
					
					ArrayList<XmlNode> spellNodes = championNode.getChildren();
					Spell[] newSpells = new Spell[4];
					if(spellNodes.size() == 4)
					{
						for(int n = 0; n < 4; n++)
						{
							XmlNode spellNode = spellNodes.get(n);
							newSpells[n] = new Spell(spellNode.getAttribute("name"), spellNode.getAttribute("key"));
						}
					}
					else
					{
						writeToLog("Incorrect number of spells on champion: "+champKey, LoggingType.WARNING);
					}
					
					spells.put(champID, newSpells);
				}
			}
		}
		catch(Exception e)
		{
			writeToLog("Failed to load champion database", LoggingType.ERROR);
			writeStackTrace(e);
			return false;
		}
		
		return true;
	}
	
	public static Spell[] getSpells(Integer championID)
	{
		if(spells != null)
			return spells.get(championID);
		return null;
	}
	
	public static int getDatabaseVersion()
	{
		return databaseVersion;
	}
}
