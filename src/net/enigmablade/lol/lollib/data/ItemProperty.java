package net.enigmablade.lol.lollib.data;

public enum ItemProperty
{
	//Types
	ATTACK_DAMAGE, ABILITY_POWER, 
	ATTACK_SPEED, COOLDOWN,
	ARMOR, MAGIC_RES,
	HEALTH, HEALTH_REGEN,
	MANA, MANA_REGEN,
	ARMOR_PEN, MAGIC_PEN,
	LIFESTEAL, SPELLVAMP,
	CRITICAL, TENACITY,
	AURA, MOVEMENT, CONSUMABLE,
	//Champions
	VIKTOR, RENGAR,
	//Game modes
	ANY_MODE, CLASSIC_MODE, TWISTED_MODE, DOMINION_MODE, ALL_MID_MODE;
	
	public static ItemProperty getModeProperty(String com)
	{
		if("sr".equals(com))
			return CLASSIC_MODE;
		else if("tt".equals(com))
			return TWISTED_MODE;
		else if("dom".equals(com))
			return DOMINION_MODE;
		else if("pg".equals(com))
			return ALL_MID_MODE;
		return null;
	}
	
	public static ItemProperty getFilterProperty(String id)
	{
		/* Pre-version 0.0.1.0
		if("11".equals(id))
			return ATTACK_DAMAGE;
		else if("7".equals(id))
			return ABILITY_POWER;
		else if("12".equals(id))
			return ATTACK_SPEED;
		else if("15".equals(id))
			return COOLDOWN;
		else if("9".equals(id))
			return ARMOR;
		else if("10".equals(id))
			return MAGIC_RES;
		else if("5".equals(id))
			return HEALTH;
		else if("4".equals(id))
			return HEALTH_REGEN;
		else if("8".equals(id))
			return MANA;
		else if("3".equals(id))
			return MANA_REGEN;
		else if("21".equals(id))
			return ARMOR_PEN;
		else if("18".equals(id))
			return MAGIC_PEN;
		else if("13".equals(id))
			return LIFESTEAL;
		else if("24".equals(id))
			return SPELLVAMP;
		else if("6".equals(id))
			return CRITICAL;
		else if("23".equals(id))
			return TENACITY;
		else if("1".equals(id))
			return MOVEMENT;
		else if("14".equals(id))
			return CONSUMABLE;*/
		if("11".equals(id))
			return ATTACK_DAMAGE;
		else if("7".equals(id))
			return ABILITY_POWER;
		else if("13".equals(id))
			return ATTACK_SPEED;
		else if("16".equals(id))
			return COOLDOWN;
		else if("9".equals(id))
			return ARMOR;
		else if("10".equals(id))
			return MAGIC_RES;
		else if("5".equals(id))
			return HEALTH;
		else if("4".equals(id))
			return HEALTH_REGEN;
		else if("8".equals(id))
			return MANA;
		else if("3".equals(id))
			return MANA_REGEN;
		else if("23".equals(id))
			return ARMOR_PEN;
		else if("20".equals(id))
			return MAGIC_PEN;
		else if("14".equals(id))
			return LIFESTEAL;
		else if("26".equals(id))
			return SPELLVAMP;
		else if("6".equals(id))
			return CRITICAL;
		else if("25".equals(id))
			return TENACITY;
		else if("19".equals(id))
			return AURA;
		else if("1".equals(id))
			return MOVEMENT;
		else if("15".equals(id))
			return CONSUMABLE;
		return null;
	}
}
