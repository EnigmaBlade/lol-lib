package net.enigmablade.lol.lollib.ui.pretty;

import javax.swing.*;

import net.enigmablade.lol.lollib.ui.pretty.uis.*;

public class PrettyButton extends JButton
{
	public PrettyButton()
	{
		this("");
	}
	
	public PrettyButton(String text)
	{
		super(text);
		
		setUI(new PrettyButtonUI());
		setOpaque(false);
	}
}
