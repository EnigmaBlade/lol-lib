package net.enigmablade.lol.lollib.ui.pretty;

import java.awt.*;
import javax.swing.*;

import net.enigmablade.lol.lollib.ui.*;
import net.enigmablade.lol.lollib.ui.pretty.uis.*;

public class PrettySideButton extends JButton
{
	private boolean top, bottom, left, right;
	
	public PrettySideButton(boolean top, boolean bottom, boolean left, boolean right)
	{
		this("", top, bottom, left, right);
	}
	
	public PrettySideButton(String text, boolean top, boolean bottom, boolean left, boolean right)
	{
		super(text);
		
		this.top = top;
		this.bottom = bottom;
		this.left = left;
		this.right = right;
		
		setUI(new PrettySideButtonUI());
		setOpaque(false);
	}
	
	public class PrettySideButtonUI extends PrettyButtonUI
	{
		@Override
		protected void paintBackground(Graphics g, JComponent c)
		{
			super.paintBackground(g, c);
			
			//g.setColor(Color.magenta);
			if(!top || !left) //top-left
				g.fillRect(0, 0, R, R);
			if(!top || !right) //top-right
				g.fillRect(c.getWidth()-R, 0, R, R);
			if(!bottom || !left) //bottom-left
				g.fillRect(0, c.getHeight()-R, R, R);
			if(!top || !right) //bottom-right
				g.fillRect(c.getWidth()-R, c.getHeight()-R, R, R);
		}
		
		@Override
		protected void paintBorder(Graphics g, JComponent c)
		{
			int topAdj = top ? 0 : R;
			int botAdj = bottom ? 0 : R;
			int leftAdj = left ? 0 : R;
			int rightAdj = right ? 0 : R;
			
			//Draw main border
			Shape save = g.getClip();
			
			g.setClip(new Rectangle(leftAdj, topAdj, getWidth()-leftAdj-rightAdj, getHeight()-topAdj-botAdj));
			g.setColor(UIUtil.COMPONENT_BORDER);
			//g.setColor(Color.magenta);
			g.drawRoundRect(0, 0, c.getWidth()-1, c.getHeight()-1, R, R);
			
			g.setClip(save);
			
			//Draw extra borders
			if(!left)
			{
				int x1 = 0;
				int x2 = R;
				
				g.drawLine(x1, 0, x2, 0);
				g.drawLine(x1, c.getHeight()-1, x2, c.getHeight()-1);
			}
			if(!right)
			{
				int x1 = c.getWidth()-1-R;
				int x2 = c.getWidth()-1;
				
				g.drawLine(x1, 0, x2, 0);
				g.drawLine(x1, c.getHeight()-1, x2, c.getHeight()-1);
			}
			//TODO: Finish the rest of the sides
		}
	}
}
