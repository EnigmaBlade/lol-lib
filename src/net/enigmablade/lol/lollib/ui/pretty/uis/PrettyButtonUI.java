package net.enigmablade.lol.lollib.ui.pretty.uis;

import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.plaf.*;
import javax.swing.plaf.basic.*;
import org.jdesktop.swingx.painter.*;

import net.enigmablade.lol.lollib.ui.*;
import net.enigmablade.lol.lollib.ui.pretty.*;

public class PrettyButtonUI extends BasicButtonUI
{
	protected static final int R = 4;
	
	private GlossPainter shader, pressedShader;
	private PrettyComponentFader fader;
	
	public PrettyButtonUI()
	{
		shader = new GlossPainter();
		shader.setPaint(UIUtil.addAlpha(Color.white, 10));
		shader.setAntialiasing(true);
		
		pressedShader = new GlossPainter(GlossPainter.GlossPosition.BOTTOM);
		pressedShader.setPaint(UIUtil.addAlpha(Color.white, 8));
		pressedShader.setAntialiasing(true);
	}
	
	@Override
	public void paint(Graphics g, JComponent c)
	{
		AbstractButton b = (AbstractButton)c;
		ButtonModel model = b.getModel();
		
		paintBackground(g, c);
		paintBorder(g, c);
		
		((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Shape oldClip = g.getClip();
		g.setClip(new Rectangle2D.Float(0, 0, c.getWidth(), c.getHeight()));
		
		if(model.isEnabled())
		{
			if(model.isArmed() || model.isPressed())
				pressedShader.paint((Graphics2D)g, null, c.getWidth(), c.getHeight());
			else
				shader.paint((Graphics2D)g, null, c.getWidth(), c.getHeight());
			
			if(fader != null && fader.isHovered())
			{
				g.setColor(fader.getOverlay());
				g.fillRoundRect(1, 1, c.getWidth()-2, c.getHeight()-2, 3, 3);
			}
		}
		
		g.setClip(oldClip);
		
		super.paint(g, c);
	}
	
	protected void paintBackground(Graphics g, JComponent c)
	{
		g.setColor(UIUtil.COMPONENT_BASE);
		g.fillRoundRect(0, 0, c.getWidth(), c.getHeight(), 4, 4);
	}
	
	protected void paintBorder(Graphics g, JComponent c)
	{
		g.setColor(UIUtil.COMPONENT_BORDER);
		g.drawRoundRect(0, 0, c.getWidth()-1, c.getHeight()-1, 4, 4);
	}
	
	@Override
	protected void paintText(Graphics g, AbstractButton c, Rectangle textRect, String text)
	{
		AbstractButton b = (AbstractButton)c;                       
		ButtonModel model = b.getModel();
		FontMetrics fm = g.getFontMetrics();
		
		if(model.isEnabled())
		{
			g.setColor(b.getForeground());
			g.drawString(text, textRect.x + getTextShiftOffset(), textRect.y + fm.getAscent() + getTextShiftOffset());
		}
		else
		{
			g.setColor(UIUtil.DISABLED_FOREGROUND.darker().darker());
			g.drawString(text, textRect.x, textRect.y + fm.getAscent());
			g.setColor(UIUtil.DISABLED_FOREGROUND);
			g.drawString(text, textRect.x - 1, textRect.y + fm.getAscent() - 1);
		}
	}
	
	//Creation interfaces
	
	@Override
	public void installUI(JComponent c)
	{
		super.installUI(c);
		
		fader = new PrettyComponentFader(c);
	}
	
	@Override
	public void uninstallUI(JComponent c)
	{
		super.uninstallUI(c);
		
		fader = null;
	}
	
	public static ComponentUI createUI(JComponent c)
	{
		return new PrettyButtonUI();
	}
}
