package net.enigmablade.lol.lollib.ui.pretty;

import java.awt.*;
import javax.swing.*;

import net.enigmablade.lol.lollib.ui.*;

public class PrettySeparator extends JSeparator
{
	public PrettySeparator()
	{
		super();
		setup();
	}
	
	public PrettySeparator(int orientation)
	{
		super(orientation);
		setup();
	}
	
	private void setup()
	{
		setForeground(UIUtil.BORDER);
		setBackground(UIUtil.BACKGROUND);
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D)g;
		
		int segW = (int)(getWidth()*0.25);
		g2.setPaint(new GradientPaint(0, 0, getBackground(), segW, 0, getForeground()));
		g2.drawLine(0, 0, segW, 0);
		
		g2.setPaint(getForeground());
		g2.drawLine(segW, 0, segW + segW*2, 0);
		
		g2.setPaint(new GradientPaint(getWidth() - segW, 0, getForeground(), getWidth(), 0, getBackground()));
		g2.drawLine(getWidth() - segW, 0, getWidth(), 0);
	}
}
