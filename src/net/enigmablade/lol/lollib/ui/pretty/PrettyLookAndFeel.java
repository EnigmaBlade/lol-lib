package net.enigmablade.lol.lollib.ui.pretty;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
//import com.sun.java.swing.plaf.windows.*;

import net.enigmablade.lol.lollib.ui.*;
import net.enigmablade.lol.lollib.ui.pretty.uis.*;

public class PrettyLookAndFeel// extends WindowsLookAndFeel
{
	public static void initLAF()
	{
		//Colors
		UIUtil.setUIBackground(UIUtil.BACKGROUND);
		UIUtil.setUISelectionBackground(UIUtil.FOREGROUND.darker());
		
		UIUtil.setUIForeground(UIUtil.FOREGROUND);
		UIUtil.setUIDisabledForeground(UIUtil.DISABLED_FOREGROUND);
		
		UIManager.put("TextField.caretForeground", new ColorUIResource(UIUtil.FOREGROUND));
		UIManager.put("TextField.border", new CompoundBorder(new LineBorder(UIUtil.BORDER), new EmptyBorder(2, 2, 2, 2)));
		UIManager.put("PasswordField.caretForeground", new ColorUIResource(UIUtil.FOREGROUND));
		UIManager.put("PasswordField.border", new CompoundBorder(new LineBorder(UIUtil.BORDER), new EmptyBorder(2, 2, 2, 2)));
		
		//Fonts
		/*Font mainFont;
		try
		{
			//mainFont = Font.createFont(Font.TRUETYPE_FONT, ResourceLoader.getResourceStream("fritz.ttf"));
			//mainFont = mainFont.deriveFont(12.0f);
			mainFont = new Font("Georgia", Font.PLAIN, 10);
		}
		catch(Exception e)
		{
			writeToLog("Failed to load font \"fritz\"", LoggingType.WARNING);
			writeStackTrace(e);
			mainFont = new Font("Times New Roman", Font.PLAIN, 12);
		}
		UIUtil.setUIFont(new FontUIResource(mainFont));*/
		
		//UIs
		UIManager.put("ScrollBarUI", PrettyScrollBarUI.class.getName());
		UIManager.put("ButtonUI", PrettyButtonUI.class.getName());
	}
	
	/*@Override
	protected void initClassDefaults(UIDefaults table)
	{
		super.initClassDefaults(table);
		
		table.put("ScrollBarUI", PrettyScrollBarUI.class.getName());
	}*/
	
	/*@Override
	protected void initComponentDefaults(UIDefaults table)
	{
		super.initComponentDefaults(table);
	}
	
	@Override
	protected void initSystemColorDefaults(UIDefaults table)
	{
		super.initSystemColorDefaults(table);
		
		UIUtil.setUIBackground(UIUtil.BACKGROUND);
		UIUtil.setUISelectionBackground(UIUtil.FOREGROUND.darker());
		
		UIUtil.setUIForeground(UIUtil.FOREGROUND);
		UIUtil.setUIDisabledForeground(UIUtil.DISABLED_FOREGROUND);
	}*/

	/*@Override
	public String getDescription()
	{
		return "Pretty Look and Feel, for League of Legends applications.";
	}

	@Override
	public String getID()
	{
		return "PrettyLAF";
	}

	@Override
	public String getName()
	{
		return "Pretty Look and Feel";
	}

	@Override
	public boolean isNativeLookAndFeel()
	{
		return System.getProperty("os.name").startsWith("Windows");
	}

	@Override
	public boolean isSupportedLookAndFeel()
	{
		return System.getProperty("os.name").startsWith("Windows");
	}*/
}
