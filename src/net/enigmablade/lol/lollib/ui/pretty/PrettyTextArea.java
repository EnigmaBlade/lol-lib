package net.enigmablade.lol.lollib.ui.pretty;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import net.enigmablade.lol.lollib.ui.*;

public class PrettyTextArea extends JTextArea
{
	public PrettyTextArea()
	{
		this("");
	}
	
	public PrettyTextArea(String text)
	{
		super(text);
		
		setBorder(new EmptyBorder(2, 2, 2, 2));
		setBackground(UIUtil.BACKGROUND);
		setCaretColor(getForeground());
		
		addFocusListener(new FocusListener(){
			@Override
			public void focusLost(FocusEvent e)
			{
				setBackground(UIUtil.BACKGROUND);
			}
			
			@Override
			public void focusGained(FocusEvent e)
			{
				setBackground(UIUtil.adjust(UIUtil.BACKGROUND, 6));
			}
		});
	}
}
