package net.enigmablade.lol.lollib.ui.pretty;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import net.enigmablade.lol.lollib.ui.*;

public class PrettyPasswordField extends JPasswordField
{
	public PrettyPasswordField()
	{
		this("");
	}
	
	public PrettyPasswordField(String text)
	{
		super(text);
		
		setBorder(new CompoundBorder(new LineBorder(UIUtil.BORDER), new EmptyBorder(2, 2, 2, 2)));
		setBackground(UIUtil.BACKGROUND);
		setCaretColor(getForeground());
		
		addFocusListener(new FocusListener(){
			@Override
			public void focusLost(FocusEvent e)
			{
				setBackground(UIUtil.BACKGROUND);
			}
			
			@Override
			public void focusGained(FocusEvent e)
			{
				setBackground(UIUtil.adjust(UIUtil.BACKGROUND, 6));
			}
		});
	}
}
