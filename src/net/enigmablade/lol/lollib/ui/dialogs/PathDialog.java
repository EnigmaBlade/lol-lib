package net.enigmablade.lol.lollib.ui.dialogs;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.plaf.basic.*;
import net.enigmablade.paradoxion.util.*;
import net.enigmablade.lol.lollib.io.*;
import net.enigmablade.lol.lollib.io.pathhelpers.platforms.*;
import net.enigmablade.lol.lollib.io.pathhelpers.regions.*;



public class PathDialog extends JDialog
{
	private static PathDialog dialogInstance = null;
	
	//GUI
	private JLabel validLabel;
	private JComboBox<GamePath> pathTextField;
	private JButton browseButton;
	private JButton saveButton, cancelButton;
	
	private JFileChooser pathFileChooser;
	
	//Data
	private GamePath originalPath, currentPath;
	private boolean valid;
	private Platform platform;
	
	public PathDialog(Platform plat)
	{
		platform = plat;
		
		setTitle("Change League of Legends Directory");
		setIconImage(ResourceLoader.getIcon());
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent evt)
			{
				cancel();
			}
		});
		setModal(true);
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		initComponents();
	}
	
	@SuppressWarnings("unchecked")
	private void initComponents()
	{
		setMinimumSize(new Dimension(425, 120));
		setMaximumSize(new Dimension(Integer.MAX_VALUE, 120));
		setResizable(false);
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(2, 2, 2, 2));
		setContentPane(contentPane);
		
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel topPanel = new JPanel();
		contentPane.add(topPanel, BorderLayout.NORTH);
		topPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 2));
		
		JLabel descLabel = new JLabel("Select the League of Legends installation location:");
		topPanel.add(descLabel);
		
		validLabel = new JLabel("");
		topPanel.add(validLabel);
		
		JPanel centerPanel = new JPanel();
		contentPane.add(centerPanel, BorderLayout.CENTER);
		GridBagLayout gbl_centerPanel = new GridBagLayout();
		gbl_centerPanel.columnWidths = new int[]{45, 0, 0};
		gbl_centerPanel.rowHeights = new int[]{41, 0};
		gbl_centerPanel.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_centerPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		centerPanel.setLayout(gbl_centerPanel);
		
		pathTextField = new JComboBox<GamePath>();
		pathTextField.setRenderer(new BasicComboBoxRenderer(){
			@SuppressWarnings("rawtypes")
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
			{
				if(value instanceof GamePath)
					value = ((GamePath)value).getPath();
				return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			}
		});
		pathTextField.setEditable(true);
		GridBagConstraints gbc_pathTextField = new GridBagConstraints();
		gbc_pathTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_pathTextField.insets = new Insets(0, 0, 0, 5);
		gbc_pathTextField.gridx = 0;
		gbc_pathTextField.gridy = 0;
		centerPanel.add(pathTextField, gbc_pathTextField);
		
		browseButton = new JButton("...");
		browseButton.setPreferredSize(new Dimension(35, 23));
		GridBagConstraints gbc_browseButton = new GridBagConstraints();
		gbc_browseButton.anchor = GridBagConstraints.EAST;
		gbc_browseButton.gridx = 1;
		gbc_browseButton.gridy = 0;
		centerPanel.add(browseButton, gbc_browseButton);
		browseButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt)
			{
				browsePath();
			}
		});
		
		JPanel bottomPanel = new JPanel();
		contentPane.add(bottomPanel, BorderLayout.SOUTH);
		bottomPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel buttonPanel = new JPanel();
		bottomPanel.add(buttonPanel, BorderLayout.EAST);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 2));
		
		saveButton = new JButton("Save");
		saveButton.setPreferredSize(new Dimension(86, 23));
		buttonPanel.add(saveButton);
		saveButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt)
			{
				close();
			}
		});
		
		cancelButton = new JButton("Cancel");
		buttonPanel.add(cancelButton);
		cancelButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt)
			{
				cancel();
			}
		});
	}
	
	//UI methods
	
	private void cancel()
	{
		pathTextField.getEditor().setItem(originalPath != null ? originalPath.getPath() : "");
		close();
	}
	
	//Function methods
	
	private void browsePath()
	{
		if(pathFileChooser == null)
		{
			pathFileChooser = new JFileChooser();
			pathFileChooser.setDialogType(JFileChooser.CUSTOM_DIALOG);
			pathFileChooser.setDialogTitle("Select");
			pathFileChooser.setApproveButtonToolTipText("Select your League of Legends base directory");
			pathFileChooser.setMultiSelectionEnabled(false);
			pathFileChooser.setDragEnabled(false);
			pathFileChooser.setFileSelectionMode(platform == Platform.MACOSX ? JFileChooser.FILES_ONLY : JFileChooser.DIRECTORIES_ONLY);
			pathFileChooser.setAcceptAllFileFilterUsed(true);
			GameFileFilter filter = new GameFileFilter(platform);
			pathFileChooser.addChoosableFileFilter(filter);
			pathFileChooser.setFileFilter(filter);
			File temp = new File(".");
			try
			{
				temp = new File(temp.getCanonicalPath());
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			pathFileChooser.setCurrentDirectory(temp);
		}
		if(pathFileChooser.showDialog(this, "Select") == JFileChooser.APPROVE_OPTION)
		{
			File selected = pathFileChooser.getSelectedFile();
			setPath(selected.getAbsolutePath());
		}
	}
	
	private void setPath(String path)
	{
		if(path != null)
		{
			pathTextField.getEditor().setItem(path);
			if(valid = GameFileFilter.isValidDirectory(currentPath = new GamePath(path, Platform.WINDOWS, Region.NA_EU)))
				validLabel.setText("valid NA/EU directory");
			else if(valid = GameFileFilter.isValidDirectory(currentPath = new GamePath(path, Platform.WINDOWS, Region.SEA)))
				validLabel.setText("valid SEA directory");
			else if((currentPath = null) == null && path.length() > 0)
				validLabel.setText("invalid directory");
			
			if(valid)
				validLabel.setForeground(new Color(0, 150, 0));
			else
				validLabel.setForeground(new Color(150, 0, 0));
			saveButton.setEnabled(valid);
		}
	}
	
	private void setHistory(GamePath[] history)
	{
		DefaultComboBoxModel<GamePath> model = new DefaultComboBoxModel<GamePath>();
		if(history != null)
			for(GamePath path : history)
				model.addElement(path);
		pathTextField.setModel(model);
	}
	
	//Use methods
	
	public void open(GamePath oldPath, GamePath[] history)
	{
		originalPath = oldPath;
		setPath(originalPath != null ? originalPath.getPath() : null);
		setHistory(history);
		setVisible(true);
	}
	
	public void close()
	{
		dispose();
	}
	
	public static GamePath open(Component parent, Platform plat, GamePath oldPath, GamePath[] history)
	{
		if(dialogInstance == null)
			dialogInstance = new PathDialog(plat);
		dialogInstance.setLocationRelativeTo(parent);
		dialogInstance.open(oldPath, history);
		
		return dialogInstance.currentPath;
	}
}
