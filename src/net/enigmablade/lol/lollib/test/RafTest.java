package net.enigmablade.lol.lollib.test;

import java.io.*;
import java.nio.*;
import com.jcraft.jzlib.*;
import net.enigmablade.lol.lollib.io.raf.*;

public class RafTest
{
	public static void main(String[] args)
	{
		String rafPath = "C:\\Riot Games\\League of Legends PBE\\RADS\\projects\\lol_game_client\\filearchives\\0.0.0.165\\Archive_234325296.raf";
		
		PathList pathList;
		FileList fileList;
		
		try
		{
			RandomAccessFile rafFile = new RandomAccessFile(rafPath, "r");
			
				
			//Magic number
			byte[] magicNumber = read(rafFile, 4, true);
			System.out.println("Magic number: "+bytesToHex(magicNumber));
			
			//Version
			byte[] version = read(rafFile, 4, true);
			System.out.println("Version: "+bytesToHex(version));
			
			//Manager index
			byte[] managerIndex = read(rafFile, 4, true);
			System.out.println("Manager index: "+bytesToHex(managerIndex));
			
			//File list offset
			byte[] fileListOffsetB = read(rafFile, 4, true);
			System.out.println("File list offset: "+bytesToHex(fileListOffsetB));
			int fileListOffset = toInt(fileListOffsetB);
			System.out.println("\t"+fileListOffset);
			
			//Path list offset
			byte[] pathListOffsetB = read(rafFile, 4, true);
			System.out.println("Path list offset: "+bytesToHex(pathListOffsetB));
			int pathListOffset = toInt(pathListOffsetB);
			System.out.println("\t"+pathListOffset);
			
			System.out.println();
			
			//Read files
			offset = fileListOffset;
			
				//File list size
			byte[] fileListCountB = read(rafFile, 4, true);
			System.out.println("File list count: "+bytesToHex(fileListCountB));
			int fileListCount = toInt(fileListCountB);
			System.out.println("\t"+fileListCount);
			
			System.out.println();
			
			fileList = new FileList(fileListCount);
			
			for(int n = 0; n < fileListCount; n++)
			{
				//Path hash
				byte[] pathHashB = read(rafFile, 4, true);
				int pathHash = toInt(pathHashB);
				System.out.println("Path hash: "+bytesToHex(pathHashB)+" ("+pathHash+")");
				
				//Data offset
				byte[] dataOffsetB = read(rafFile, 4, true);
				int dataOffset = toInt(dataOffsetB);
				System.out.println("Data offset: "+bytesToHex(dataOffsetB)+" ("+dataOffset+")");
				
				//Data size
				byte[] dataSizeB = read(rafFile, 4, true);
				int dataSize = toInt(dataSizeB);
				System.out.println("Data size: "+bytesToHex(dataSizeB)+" ("+dataSize+")");
				
				//Path list index
				byte[] pathListIndexB = read(rafFile, 4, true);
				int pathListIndex = toInt(pathListIndexB);
				System.out.println("Path list index: "+bytesToHex(pathListIndexB)+" ("+pathListIndex+")");
				
				System.out.println();
				
				fileList.addEntry(pathHash, dataOffset, dataSize, pathListIndex);
			}
			
			System.out.println();
			
			//Read paths
			offset = pathListOffset;
			
				//Path list size
			byte[] pathListSizeB = read(rafFile, 4, true);
			System.out.println("Path list size: "+bytesToHex(pathListSizeB));
			int pathListSize = toInt(pathListSizeB);
			System.out.println("\t"+pathListSize);
			
				//Path list count
			byte[] pathListCountB = read(rafFile, 4, true);
			System.out.println("Path list count: "+bytesToHex(pathListCountB));
			int pathListCount = toInt(pathListCountB);
			System.out.println("\t"+pathListCount);
			
			//System.out.println();
			
			pathList = new PathList(pathListCount);
			
			int[] pathOffsets = new int[pathListCount];
			int[] pathLengths = new int[pathListCount];
			for(int n = 0; n < pathListCount; n++)
			{
				//Path offset
				byte[] pathOffsetB = read(rafFile, 4, true);
				int pathOffset = toInt(pathOffsetB);
				System.out.println("Path offset: "+bytesToHex(pathOffsetB)+" ("+pathOffset+")");
				pathOffsets[n] = pathOffset;
				
				//Path length
				byte[] pathLengthB = read(rafFile, 4, true);
				int pathLength = toInt(pathLengthB);
				System.out.println("Path length: "+bytesToHex(pathLengthB)+" ("+pathLength+")");
				pathLengths[n] = pathLength;
			}
			
			for(int n = 0; n < pathListCount; n++)
			{
				offset = pathListOffset+pathOffsets[n];
				
				byte[] pathB = read(rafFile, pathLengths[n]-1, false);
				String path = new String(pathB);
				System.out.println("Path: "+path);
				pathList.addEntry(pathOffsets[n], pathLengths[n], path);
			}
			
			rafFile.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return;
		}
		
		offset = 0;
		System.out.println();
		
		try
		{
			RandomAccessFile datFile = new RandomAccessFile(rafPath+".dat", "r");
			
			File outputDir = new File("RAF_Contents");
			System.out.println("Output: "+outputDir.getAbsolutePath());
			outputDir.mkdirs();
			
			for(FileEntry entry : fileList)
			{
				//Entry path
				PathEntry entryPath = pathList.get(entry.pathIndex);
				if(entryPath.path.matches("^DATA/Characters/[A-za-z]+/Recommended/.*\\.json$"))
				{
					File entryFile = new File(outputDir.getAbsoluteFile()+"\\"+entryPath.path);
					System.out.println(entryFile.getAbsolutePath());
					entryFile.getParentFile().mkdirs();
					
					//Entry bytes
					offset = entry.offset;
					byte[] entryBytes = read(datFile, entry.size, false);
					
					//String test = new String(entryBytes);
					//System.out.println(test);
					
					//Decompress
					ByteArrayInputStream in = new ByteArrayInputStream(entryBytes);
					InflaterInputStream zIn = new InflaterInputStream(in);
					
					//ObjectInputStream objIn = new ObjectInputStream(zIn);
					//System.out.println(objIn.readObject());
					
					FileOutputStream out = new FileOutputStream(entryFile);
					copyStream(zIn, out);
				}
			}
			
			datFile.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
	}
	
	//Reading
	
	private static long offset = 0;
	
	private static byte[] read(RandomAccessFile file, int len, boolean revEnd) throws IOException
	{
		byte[] b = new byte[len];
		file.seek(offset);
		int read = file.read(b);
		if(read != -1)
		{
			offset += read;
			if(revEnd)
				reverseEndian(b);
			//System.out.println("Read: "+read+" bytes, offset = "+offset);
		}
		else
			System.out.println("Warning: EOF");
		return b;
	}
	
	private static byte[] reverseEndian(byte[] bytes)
	{
		for(int i = 0, j = bytes.length-1; i < j; i++, j--)
		{
			byte b = bytes[i];
			bytes[i] = bytes[j];
			bytes[j] = b;
		}
		return bytes;
	}
	
	private static int toInt(byte[] bytes)
	{
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		IntBuffer intBuffer = buffer.asIntBuffer();
		return intBuffer.get();
	}
	
	public static void copyStream(InputStream input, OutputStream output) throws IOException
	{
		byte[] buffer = new byte[1024];
		int bytesRead;
		while((bytesRead = input.read(buffer)) != -1)
			output.write(buffer, 0, bytesRead);
	}
	
	//Helper
	
	public static String bytesToHex(byte[] bytes)
	{
		StringBuilder sb = new StringBuilder();
		for(byte b : bytes)
			sb.append(String.format("%02X", b));
		return sb.toString();
	}
}
